﻿
function Paging(Table) {
    var PageRecord = 5;
    var PageCount = 0;
    var RecordCount = 0.0;
    //var rows = document.getElementById(Table).rows;
    PageCount = parseInt(document.getElementById(Table).rows.length - 1) / PageRecord;
    var array = (PageCount + '').split('.');
    if (array[1] !== undefined)
        PageCount = parseInt(array[0]) + 1;
    $('#sp_1_grid-pager').html(PageCount);
    ShowRecord(1, PageRecord, Table);
    //alert(PageCount);

}
function NextPage(Table) {    
    var CurrentPage = parseInt($('#sp_0_grid-pager').text().trim());
    var DestinationPage = CurrentPage + 1;
    if (DestinationPage <= parseInt($('#sp_1_grid-pager').text().trim())) {
        var PagerowCount = 5;
        ShowRecord((CurrentPage * 5) + 1, DestinationPage * 5, Table);
        $('#sp_0_grid-pager').text(DestinationPage);
    }
}
function PreviousPage(Table) {
    var CurrentPage = parseInt($('#sp_0_grid-pager').text().trim());
    var DestinationPage = CurrentPage - 1;
    if (DestinationPage == 1) {
        ShowRecord(1, 5, Table);
        $('#sp_0_grid-pager').text(DestinationPage);
    }

    else if (DestinationPage > 1 && DestinationPage <= parseInt($('#sp_1_grid-pager').text().trim())) {
        var PagerowCount = 10;
        var from = (5 * (CurrentPage - 1) + 1) - 5;
        var to = (5 * CurrentPage) - 5;
        ShowRecord(from, to, Table);
        $('#sp_0_grid-pager').text(DestinationPage);
    }
}
function ShowRecord(from, to, Table) {
    var rows = document.getElementById(Table).rows;
    for (var i = 1; i < rows.length; i++) {
        if (i < from || i > to)
            rows[i].style.display = 'none';
        else
            rows[i].style.display = '';
    }
}
function FirstPage(Table) {
    ShowRecord(1, 5, Table);
    $('#sp_0_grid-pager').text("1");
}
function LastPage(Table) {
    var PageRecord = 5;
    var PageCount = 0;
    PageCount = parseInt(document.getElementById(Table).rows.length - 1) / PageRecord;
    var array = (PageCount + '').split('.');
    if (array[1] !== undefined)
        PageCount = parseInt(array[0]) + 1;
    var LastPage = parseInt();
    var from = (PageRecord * (parseInt($('#sp_1_grid-pager').text().trim()) - 1)) + 1;
    var to = parseInt(document.getElementById(Table).rows.length - 1);
    ShowRecord(from, to, Table);
    $('#sp_0_grid-pager').text(PageCount);
}

//******* Searching In Grid
$(document).ready(function () {
    $('.Search').keyup(function () {
        var TableID = $(this).attr('data-toggle');
        searchTable($(this).val(), TableID);
        if ($(this).val().trim() == '') {
            ShowRecord(1, 5, TableID)
        }
    });
});
function searchTable(inputVal, TableID) {
    var table = $('#' + TableID);
    table.find('tr').each(function (index, row) {
        var allCells = $(row).find('td');
        if (allCells.length > 0) {
            var found = false;
            allCells.each(function (index, td) {
                var regExp = new RegExp(inputVal, 'i');
                if (regExp.test($(td).text())) {
                    found = true;
                    return false;
                }
            });
            if (found == true) $(row).show(); else $(row).hide();
        }
    });
}