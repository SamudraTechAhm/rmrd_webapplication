﻿

var jsonnewID = 0;
var RTCODE;

function GETRouteDate() {

    var action = null;
    action = "BindEffDate";
    $.ajax({
        type: "POST",
        url: "DropDown.aspx/BindDropDownList",
        data: "{ Action: '" + action + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {

            var json = JSON.parse(data.d);
            $.each(json, function (key, value) {
                $("#drpRouteDate").append($("<option></option>").val(value.effcode).html(value.EffDate));
            });
        },
        failure: function (response) {
            alert("failure");
        },
        error: function (response) {
            alert("error");
        }
    });
}

function SelectData() {
    $.ajax({
        type: "POST",
        url: "RouteMasterEdit.aspx/SelectData",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {

            var TableGroupName = data.d;
            var TableArray = TableGroupName.split('^');

            var jsonVihical = jQuery.parseJSON(TableArray[0]);
            var jsonOfficer = jQuery.parseJSON(TableArray[1]);
            var jsonCenter = jQuery.parseJSON(TableArray[2]);
            jsonnewID = jQuery.parseJSON(TableArray[3]);

            $.each(jsonVihical, function (key, value) {
                $("#ddlTruckNo").append($("<option></option>").val(value.VehicalCode).html(value.VehicalName));
            });
            $.each(jsonOfficer, function (key, value) {
                $("#ddlFieldOff").append($("<option></option>").val(value.Code).html(value.Name));
            });
            $.each(jsonCenter, function (key, value) {
                $("#ddlCenter").append($("<option></option>").val(value.cntCode).html(value.cntName));
            });
            $.each(jsonnewID, function (key, value) {
                RTCODE = value.RtCode
                $("#txtRouteID").val(value.RtCode)
            });



            //$("#txtGuranter2DivName").val(value)


        },
        failure: function (response) {
            alert("failure");
        },
        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            alert(err.Message);
        }
    });
}
function SelecrRouteCode() {
    $.ajax({
        type: "POST",
        url: "RouteMasterEdit.aspx/SelectData",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {

            var TableGroupName = data.d;
            var TableArray = TableGroupName.split('^');

            //var jsonVihical = jQuery.parseJSON(TableArray[0]);
            //var jsonOfficer = jQuery.parseJSON(TableArray[1]);
            //var jsonCenter = jQuery.parseJSON(TableArray[2]);
            jsonnewID = jQuery.parseJSON(TableArray[3]);


            $.each(jsonnewID, function (key, value) {
                RTCODE = value.RtCode
                $("#txtRouteID").val(value.RtCode)
            });

        }
    });
}
function LoadDataToForm(_ID) {
    $.ajax({
        type: "POST",
        url: "RouteMasterEdit.aspx/LoadToForm",
        data: '{RouteCode: "' + _ID + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {
            var Route = response.d;

            var json = jQuery.parseJSON(Route);
            $("#txtRouteID").val(json.RouteCode);
            $("#txtRouteName").val(json.RouteName);
            $("#MornigTime").val(json.SchTimeMor);
            $("#timepicker1").val(json.SchTimeEve);
            $("#txtMorningKM").val(json.RtKMM);
            $("#txtEveKM").val(json.RtKME);
            var a = json.CMM
            var b = json.VihicalNo
            var c = json.FieldOfficer

            $("#ddlCenter").val(a);
            $("#ddlTruckNo").val(b);
            $("#ddlFieldOff").val(c);
            if (json.RtMixMilk == true)
            {
                $("#ChkIsMilk").prop("checked", true)
            }
            else if (json.RtMixMilk == false) {
                $("#ChkIsMilk").prop("checked", false)
            }
        },
        error: function (response) {
            alert("error");
        },
        failure: function (response) {
            alert("failure");
        }
    });
}

function btnSave_click() {

    var operation = $('#btnSave').attr('value');

    if (operation == "Save") {

        SaveData("INSERT");
    }
    else if (operation == "UPDATE") {
        SaveData("UPDATE");
    }
}

function RouteObjCreate() {
    var _Route = {};
    _Route.EffCode = 0;
    _Route.RtCode = 0;
    _Route.RtName = '';
    _Route.RtSchTimeM = 0;
    _Route.RtSchTimeE = 0;
    _Route.RtKMM = 0;
    _Route.RtKME = 0;
    _Route.IsMixMilk = 0;
    _Route.VihicalNo = 0;
    _Route.FieldOfficer = 0;
    _Route.CntCode = 0;
    _Route.Action = '';

    return _Route;
}

function SaveData(action) {
    
    if ($("#txtMorningKM").val().indexOf('.') != -1 || $("#txtEveKM").val().indexOf('.') != -1)
    {
    
        if ($("#txtMorningKM").val().length <= 1 || $("#txtEveKM").val().length <= 1) {
            
            $("#txtMorningKM").val("")
            $("#txtEveKM").val("")
            $(".required").blur();
            
    }        
    }

    if ($("#txtRouteName").val().trim() == "" || $("#ddlCenter").val().trim() == "0" || $("#ddlTruckNo").val().trim() == "0" || $("#ddlFieldOff").val().trim() == "0") {
        $(".required").blur();
    }
    else {
        var Route = RouteObjCreate();

        Route.EffCode = GetParameterValues('EFFCODE');
        Route.RtCode = $("#txtRouteID").val().trim();
        Route.RtName = $("#txtRouteName").val().trim();
        Route.RtKMM = $("#txtMorningKM").val().trim();
        Route.RtKME = $("#txtEveKM").val().trim();
        Route.RtSchTimeM = $("#MornigTime").val().trim();
        Route.RtSchTimeE = $("#timepicker1").val().trim();
        Route.CntCode = $("#ddlCenter  option:selected").val();
        if ($("#ChkIsMilk").is(":checked")) {
            Route.IsMixMilk = 1;
        }
        else {
            Route.IsMixMilk = 0;
        }

        Route.VihicalNo = $("#ddlTruckNo  option:selected").val();
        Route.FieldOfficer = $("#ddlFieldOff  option:selected").val();
        Route.Action = action
        $.ajax({
            type: "POST",
            url: "RouteMasterEdit.aspx/InsertRouteDetail",
            data: JSON.stringify({ 'Route': Route }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {

                if (Route.Action == "UPDATE") {
                    window.location.href = "RouteMaster.aspx";
                }
                else if (Route.Action == "INSERT") {

                    ohSnap('Route Master Data Submit Successfully !!');
                    ClearAll();
                    $('#timepicker1').timepicker('setTime', new Date());
                    $('#MornigTime').timepicker('setTime', new Date());

                }
                $("#txtRouteName").focus();
                SelecrRouteCode();
                $("#txtRouteID").val(RTCODE)



            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
}

function BtnBack_Click() {
    window.location = "RouteMaster.aspx"
}

function ClearAll() {

    $("#txtRouteName").val("")
    $("#ddlCenter").val(0)
    $("#ddlTruckNo").val(0)
    $("#ddlFieldOff").val(0)
    $("#timepicker1").val("")
    $("#MornigTime").val("")
    $("#txtMorningKM").val("")
    $("#txtEveKM").val("")
    $("#ChkIsMilk").prop("checked", false)
    //$("#ChkIsMilk").val(0)


}

