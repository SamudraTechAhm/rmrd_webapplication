﻿
function GETContractor() {

    var action = null;
    action = "BindContractor";
    $.ajax({
        type: "POST",
        url: "DropDown.aspx/BindDropDownList",
        data: "{ Action: '" + action + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {
            var TableGroupName = data.d;
            var TableArray = TableGroupName.split('^');
            var jsonState = jQuery.parseJSON(TableArray[0]);
            var jsonMaxID = jQuery.parseJSON(TableArray[1]);

            $.each(jsonState, function (key, value) {
                $("#ddlContractor").append($("<option></option>").val(value.RtcCode).html(value.RtcName));
            });
            $.each(jsonMaxID, function (key, value) {
                $("#txtVehicalID").val(value.MaxFS);
            });


        },
        failure: function (response) {
            alert("failure");
        },
        error: function (response) {
            alert("error");
        }
    });
}
function getid()
{
    var action = null;
    action = "BindContractor";
    $.ajax({
        type: "POST",
        url: "DropDown.aspx/BindDropDownList",
        data: "{ Action: '" + action + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {
            var TableGroupName = data.d;
            var TableArray = TableGroupName.split('^');
       
            var jsonMaxID = jQuery.parseJSON(TableArray[1]);

            $.each(jsonMaxID, function (key, value) {
                $("#txtVehicalID").val(value.MaxFS);
            });


        },
        failure: function (response) {
            alert("failure");
        },
        error: function (response) {
            alert("error");
        }
    });
}
function FillGrid() {
    $.ajax({
        type: "POST",
        url: "TenkerMaster.aspx/GetTenkar",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {

            var json = JSON.parse(data.d);
            $('#TblTenkarDetail tbody tr').remove();
            $.each(json, function (key, value) {
                //var col1 = '<tr><td onclick="Edit_Row($(this))" class="td width-100 btn btn-block btn-flat center"><i class="ace-icon fa fa-edit blue bigger-150"></i></td>';
                var col1 = '<tr><td onclick="Edit_Row($(this))" class="td width-100 btn btn-block btn-flat center"><i class="ace-icon fa fa-pencil-square-o bigger-150"></i></td>';
                var col2 = '<td class="td code center">' + value.VehicalCode + '</td>';
                var col3 = '<td class="td  name left">' + value.VehicalName + '</td>';
                var col4 = '<td class="td  type left">' + value.RtcName + '</td>';
                var col5 = '<td class="td  rtcname center ">' + value.type + '</td>';
                var col6 = '<td class="td  capacity left ">' + value.Capacity  + '</td>';
                var col7 = '<td class="td  capacity left ">' + value.MobileNo1 + '</td>';
                $("tbody").append(col1 + col2 + col3 + col4 + col5 + col6 + col7);
            });
            Paging('TblTenkarDetail');
        },
        failure: function (response) {
            alert("failure");
        },
        error: function (response) {
            alert("error");
        }
    });
    $("#BtnAddNew").focus();
}

function Edit_Row(row) {
    var $item = row.closest("tr").find(".code").text();
    var url = "TenkarMasterEdit.aspx?id=" + $item;
    window.location = url;
}
function LoadDataToForm(_ID) {

    $.ajax({
        type: "POST",
        url: "TenkarMasterEdit.aspx/LoadToForm",
        data: '{VehicalCode: "' + _ID + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {
            var Parameter = response.d;            
            var json = jQuery.parseJSON(Parameter);
            $("#txtVehicalID").val(json.VehicalCode);
            $("#txtVName").val(json.VehicalName);
            $("#txtType").val(json.Type);
            $("#txtCapacity").val(json.Capacity);
            $("#ddlContractor").val(json.RtcCode);
            $("#txtMobile").val(json.MobileNo1);           

        },
        error: function (response) {
            alert("error");
        },
        failure: function (response) {
            alert("failure");
        }
    });
}

function btnSave_click()
{
    var Operation = $("#btnSave").val()
    
    if (Operation == "Save")
    {
        SaveData("INSERT")
    }
    if (Operation == "UPDATE") {
        SaveData("UPDATE")
    }
}
function objTenkarMasterData()
{
        var _Tenkar = {};
        _Tenkar.VehicalCode = 0;
        _Tenkar.VehicalName ='';
        _Tenkar.RtcCode =0;
        _Tenkar.Type = '';
        _Tenkar.MobileNo1 = 0;
        _Tenkar.Capacity = '';        
        _Tenkar.Action = '';

        return _Tenkar;
    
}
function SaveData(action) {
    
    var Tenkar = objTenkarMasterData();

    Tenkar.VehicalCode = parseInt($("#txtVehicalID").val().trim());
    Tenkar.VehicalName = $("#txtVName").val().trim();
    Tenkar.RtcCode = parseInt($("#ddlContractor  option:selected").val());
    Tenkar.Type = $("#txtType").val().trim();
    Tenkar.MobileNo1 = parseInt($("#txtMobile").val().trim());
    Tenkar.Capacity = $("#txtCapacity").val().trim();
    Tenkar.Action = action;
    $.ajax({
        type: "POST",
        url: "TenkarMasterEdit.aspx/InsertUpdateDelete",
        data: JSON.stringify({ 'Tenkar': Tenkar }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {

            if (Tenkar.Action == "UPDATE") {
                window.location.href = "TenkerMaster.aspx";
            }
            else if (Tenkar.Action == "INSERT") {
                ohSnap(data.d);
                getid();
                //ClearAll();


            }
            //$("#txtName").focus();
            //window.location.href = "SupervisiorMasterEdit.aspx";

        },
        failure: function (response) {
            alert(response.d);
        },
        error: function (xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            alert(err.Message);
        }
    });
    }
