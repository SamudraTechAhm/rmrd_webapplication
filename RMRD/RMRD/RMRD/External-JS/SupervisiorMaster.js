﻿
function GETState() {

    var action = null;
    action = "BindSTATE";
    $.ajax({
        type: "POST",
        url: "DropDown.aspx/BindDropDownList",
        data: "{ Action: '" + action + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {
            var TableGroupName = data.d;
            var TableArray = TableGroupName.split('^');
            var jsonState = jQuery.parseJSON(TableArray[0]);
            var jsonMaxID = jQuery.parseJSON(TableArray[1]);

            $.each(jsonState, function (key, value) {
                $("#ddlState").append($("<option></option>").val(value.StateId).html(value.StateName));
            });
            $.each(jsonMaxID, function (key, value) {
                $("#txtSupervisorID").val(value.MaxFS);
            });
        },
        failure: function (response) {
            alert("failure");
        },
        error: function (response) {
            alert("error");
        }
    });
}
function FillGrid() {
    
    $.ajax({
        type: "POST",
        url: "SupervisiorMaster.aspx/GetSupervisior",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {

            var json = JSON.parse(data.d);
            $.each(json, function (key, value) {
                //var col1 = '<tr><td onclick="Edit_Row($(this))" class="td width-100 btn btn-block btn-flat center"><i class="ace-icon fa fa-edit blue bigger-150"></i></td>';
                var col1 = '<tr><td onclick="Edit_Row($(this))" class="td width-100 btn btn-block btn-flat center"><i class="ace-icon fa fa-pencil-square-o bigger-150"></i></td>';
                var col2 = '<td class="td code center">' + value.FsCode + '</td>';
                var col3 = '<td class="td  left">' + value.FsName + '</td>';
                var col4 = '<td class="td  left">' + value.Designation + '</td>';
                var col5 = '<td class="td  center">' + value.Mobile + '</td>';
                var col6 = '<td class="td  center ">' + value.BirthDate + '</td>';
                var col7 = '<td class="td  left ">' + value.City + '</td>';
                $("tbody").append(col1 + col2 + col3 + col4 + col5 + col6 + col7);
            });
            Paging('TblSupervisiorDetail');
        },
        failure: function (response) {
            alert("failure");
        },
        error: function (response) {
            alert("error");
        }
    });
}
function Edit_Row(row) {
    var $item = row.closest("tr").find(".code").text();
    var url = "SupervisiorMasterEdit.aspx?id=" + $item ;
    window.location = url;
}
function BackClick() {
    window.location = "SupervisiorMaster.aspx";
}
function BtnNewClick() {
    window.location = "SupervisiorMasterEdit.aspx";
}
function BtnBackClick() {
    window.location = "WebForm1.aspx";
}

function LoadDataToForm(_ID) {
    
    $.ajax({
        type: "POST",
        url: "SupervisiorMasterEdit.aspx/LoadToForm",
        data: '{FsCode: "' + _ID + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {
            var Parameter = response.d;
            
            var json = jQuery.parseJSON(Parameter);
            $("#txtSupervisorID").val(json.FsCode);
            $("#txtName").val(json.FsName);
            $("#txtDesignation").val(json.Designation);
            $("#txtContectNo").val(json.Mobile);
            $("#txtEmail").val(json.EmailId);
            $("#txtBDate").val(json.BirthDate);
            $("#txtCity").val(json.City);
            $("#ddlState").val(json.State);
            $("#txtCountry").val(json.Country);
            $("#txtAdd1").val(json.Add1);
            $("#txtPin").val(json.PINCode);
            if (json.Gender == "Male") {
                $("#RadioMale").attr("Checked", "Checked");
            }
            else if (json.Gender == "Female") {
                $("#RadioFeMale").attr("Checked", "Checked");
            }
            
        },
        error: function (response) {
            alert("error");
        },
        failure: function (response) {
            alert("failure");
        }
    });
}

//----INSERT UPDATE DATA


function btnSave_click() {

    var operation = $('#btnSave').attr('value');
    
    if (operation == "Save") {
    
        SaveData("INSERT");
    }
    else if (operation == "UPDATE") {
        SaveData("UPDATE");
    }
}
function SupervisiorObjCreate() {
    var _Supervisior = {};
    _Supervisior.FsCode = 0;
    _Supervisior.Mobile = 0;
    _Supervisior.FsName = '';
    _Supervisior.Designation ='';
    _Supervisior.EmailId = '';
    _Supervisior.Gender = '';
    _Supervisior.BirthDate ='';
    _Supervisior.Add1 = '';
    _Supervisior.PINCode = 0;
    _Supervisior.City = '';
    _Supervisior.State = '';
    _Supervisior.Country = '';
    _Supervisior.CntCode = 0;
    _Supervisior.Action = '';

    return _Supervisior;
}
function SaveData(action) {
    if ($("#txtName").val().trim() == "" || $("#txtDesignation").val().trim() == "" || $("#txtContectNo").val().trim() == "" || $("#txtBDate").val().trim() == "" || $("#txtCity").val().trim() == "" || $("#ddlState").val().trim() == "0" || $("#txtAdd1").val().trim() == "") {
        $(".required").blur();
    }
    else {
        var Supervisior = SupervisiorObjCreate();

        Supervisior.FsCode = parseInt($("#txtSupervisorID").val().trim());
        Supervisior.Mobile = $("#txtContectNo").val().trim();
        Supervisior.FsName = $("#txtName").val().trim();
        Supervisior.Designation = $("#txtDesignation").val().trim();
        Supervisior.EmailId = $("#txtEmail").val().trim();
        Supervisior.BirthDate = DateFormat($("#txtBDate").val().trim());
        if ($("#RadioMale").is(":Checked")) {
            Supervisior.Gender = "Male"
        }
        else if ($("#RadioFeMale").is(":Checked")) {
            Supervisior.Gender = "Female"
        }
        Supervisior.Add1 = $("#txtAdd1").val().trim();
        Supervisior.PINCode = $("#txtPin").val().trim();
        Supervisior.City = $("#txtCity").val().trim();
        Supervisior.State = parseInt($("#ddlState  option:selected").val());
        Supervisior.Country = $("#txtCountry").val().trim();
        Supervisior.Action = action;
         $.ajax({
             type: "POST",
             url: "SupervisiorMasterEdit.aspx/InsertSuprevisorDetail",
             data: JSON.stringify({ 'Supervisior': Supervisior }),
             contentType: "application/json; charset=utf-8",
             dataType: "json",
             async: false,
             success: function (data) {
                 
                 if (Supervisior.Action == "UPDATE") {
                     window.location.href = "SupervisiorMaster.aspx";
                 }
                 else if (Supervisior.Action == "INSERT") {
                     ohSnap(data.d);
                     ClearAll();
                     
                     
                 }
                  $("#txtName").focus();
                  window.location.href = "SupervisiorMasterEdit.aspx";
        
             },
             failure: function (response) {
                 alert(response.d);
             },
             error: function (xhr, status, error) {
                 var err = eval("(" + xhr.responseText + ")");
                 alert(err.Message);
             }
         });
    }
    }
function DateFormat(Date) {
    var splitDate = Date.split('/');
    var date = splitDate[2] + "/" + splitDate[1] + "/" + splitDate[0];
    return date;
}
function ClearAll() {

    $("#txtName").val("")
    $("#txtDesignation").val("")
    $("#txtContectNo").val("")
    $("#txtEmail").val("")
    $("#txtBDate").val("")
    $("#txtAdd1").val("")
    $("#txtPin").val("")
    $("#txtCity").val("")
    $("#txtCity").val("")
    $("#txtCountry").val("")
    $("#RadioMale").attr("Checked", false);
    $("#RadioFeMale").attr("Checked", false);
   // $("#ChkIsMilk").prop("checked", false)
    //$("#ChkIsMilk").val(0)


}
