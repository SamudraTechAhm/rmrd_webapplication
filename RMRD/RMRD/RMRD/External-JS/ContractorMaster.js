﻿
function GETState() {

    var action = null;
    action = "BindSTATEForContractor";
    $.ajax({
        type: "POST",
        url: "DropDown.aspx/BindDropDownList",
        data: "{ Action: '" + action + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {
            var TableGroupName = data.d;
            var TableArray = TableGroupName.split('^');
            var jsonState = jQuery.parseJSON(TableArray[0]);
            var jsonMaxID = jQuery.parseJSON(TableArray[1]);

            $.each(jsonState, function (key, value) {
                $("#ddlState").append($("<option></option>").val(value.StateId).html(value.StateName));
            });
            $.each(jsonMaxID, function (key, value) {
                $("#txtSupervisorID").val(value.RtcCode);
            });
        },
        failure: function (response) {
            alert("failure");
        },
        error: function (response) {
            alert("error");
        }
    });
}
function FillGrid() {

    $.ajax({
        type: "POST",
        url: "ContractorMaster.aspx/GetContractor",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {

            var json = JSON.parse(data.d);
            $.each(json, function (key, value) {
                //var col1 = '<tr><td onclick="Edit_Row($(this))" class="td width-100 btn btn-block btn-flat center"><i class="ace-icon fa fa-edit blue bigger-150"></i></td>';
                var col1 = '<tr><td onclick="Edit_Row($(this))" class="td width-100 btn btn-block btn-flat center"><i class="ace-icon fa fa-pencil-square-o bigger-150"></i></td>';
                var col2 = '<td class="td code center">' + value.RtcCode + '</td>';
                var col3 = '<td class="td  left">' + value.RtcName + '</td>';
                var col4 = '<td class="td  center">' + value.Mobile + '</td>';
                var col5 = '<td class="td  center">' + value.BirthDate + '</td>';
                var col6 = '<td class="td  left ">' + value.Add1 + '</td>';
                var col7 = '<td class="td  left ">' + value.City + '</td>';
                $("tbody").append(col1 + col2 + col3 + col4 + col5 + col6 + col7);
            });
            Paging('TblContractorDetail');
        },
        failure: function (response) {
            alert("failure");
        },
        error: function (response) {
            alert("error");
        }
    });
}
function Edit_Row(row) {
    var $item = row.closest("tr").find(".code").text();
    var url = "ContractorMasterEdit.aspx?id=" + $item;
    window.location = url;
}
function BackClick() {
    window.location = "ContractorMaster.aspx";
}
function BtnNewClick() {
    window.location = "ContractorMasterEdit.aspx";
}
function LoadDataToForm(_ID) {

    $.ajax({
        type: "POST",
        url: "ContractorMasterEdit.aspx/LoadToForm",
        data: '{RtcCode: "' + _ID + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (response) {
            var Parameter = response.d;

            var json = jQuery.parseJSON(Parameter);
            $("#txtSupervisorID").val(json.RtcCode);
            $("#txtName").val(json.RtcName);            
            $("#txtContectNo").val(json.Mobile);
            $("#txtEmail").val(json.EmailId);
            $("#txtBDate").val(json.BirthDate);
            $("#txtCity").val(json.City);
            $("#ddlState").val(json.State);
            $("#txtCountry").val(json.Country);
            $("#txtAdd1").val(json.Add1);
            $("#txtPin").val(json.PINCode);
            if (json.Gender == "Male") {
                $("#RadioMale").attr("Checked", "Checked");
            }
            else if (json.Gender == "Female") {
                $("#RadioFeMale").attr("Checked", "Checked");
            }
            
        },
        error: function (response) {
            alert("error");
        },
        failure: function (response) {
            alert("failure");
        }
    });
}

function btnSave_click() {

    var operation = $('#btnSave').attr('value');

    if (operation == "Save") {

        SaveData("INSERT");
    }
    else if (operation == "UPDATE") {
        SaveData("UPDATE");
    }
}
function ContractorObjCreate() {
    var _Contractor = {};
    _Contractor.RtcCode = 0;
    _Contractor.Mobile = 0;
    _Contractor.RtcName = '';    
    _Contractor.EmailId = '';
    _Contractor.Gender = '';
    _Contractor.BirthDate = '';
    _Contractor.Add1 = '';
    _Contractor.PINCode = 0;
    _Contractor.City = '';
    _Contractor.State = '';
    _Contractor.Country = '';
    _Contractor.CntCode = 0;
    _Contractor.Action = '';
    return _Contractor;
}

function SaveData(action) {
    if ($("#txtName").val().trim() == "" || $("#txtContectNo").val().trim() == "" || $("#txtBDate").val().trim() == "" || $("#txtCity").val().trim() == "" || $("#ddlState").val().trim() == "0" || $("#txtAdd1").val().trim() == "") {
        $(".required").blur();
    }
    else {
        var Contractor = ContractorObjCreate();

        Contractor.RtcCode = parseInt($("#txtSupervisorID").val().trim());
        Contractor.Mobile = $("#txtContectNo").val().trim();
        Contractor.RtcName = $("#txtName").val().trim();        
        Contractor.EmailId = $("#txtEmail").val().trim();
        Contractor.BirthDate = DateFormat($("#txtBDate").val().trim());
        if ($("#RadioMale").is(":Checked")) {
            Contractor.Gender = "Male"
        }
        else if ($("#RadioFeMale").is(":Checked")) {
            Contractor.Gender = "Female"
        }
        Contractor.Add1 = $("#txtAdd1").val().trim();
        Contractor.PINCode = $("#txtPin").val().trim();
        Contractor.City = $("#txtCity").val().trim();
        Contractor.State = parseInt($("#ddlState  option:selected").val());
        Contractor.Country = $("#txtCountry").val().trim();
        Contractor.Action = action;
        $.ajax({
            type: "POST",
            url: "ContractorMasterEdit.aspx/InsertSuprevisorDetail",
            data: JSON.stringify({ 'Contractor': Contractor }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {
                
                if (Contractor.Action == "UPDATE")
                {
                    window.location.href = "ContractorMaster.aspx";
                }
                else if (Contractor.Action == "INSERT")
                {
                    ohSnap(data.d);
                    setTimeout(ClearAll(), 200000);
                    
                }
                if (Contractor.Action == "INSERT") {
                    setTimeout(window.location.href = "ContractorMasterEdit.aspx", 200000);
                }
                $("#txtName").focus();
                //window.location.href = "ContractorMasterEdit.aspx";
        
            },
            failure: function (response) {
                alert(response.d);
            },
            error: function (xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
            }
        });
    }
}
function DateFormat(Date) {
    var splitDate = Date.split('/');
    var date = splitDate[2] + "/" + splitDate[1] + "/" + splitDate[0];
    return date;
}

function ClearAll()
{
    $("#txtName").val("")
    $("#txtDesignation").val("")
    $("#txtContectNo").val("")
    $("#txtEmail").val("")
    $("#txtBDate").val("")
    $("#txtAdd1").val("")
    $("#txtPin").val("")
    $("#txtCity").val("")
    $("#txtCity").val("")
    $("#txtCountry").val("")
    $("#RadioMale").attr("Checked", false);
    $("#RadioFeMale").attr("Checked", false);

}