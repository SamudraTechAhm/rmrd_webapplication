﻿
function GETDistrict() {

    var action = null;
    action = "BindDistrict";
    $.ajax({
        type: "POST",
        url: "DropDown.aspx/BindDropDownList",
        data: "{ Action: '" + action + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {
            var TableGroupName = data.d;
            var TableArray = TableGroupName.split('^');
            var jsonState = jQuery.parseJSON(TableArray[0]);
            var jsonMaxID = jQuery.parseJSON(TableArray[1]);

            $.each(jsonState, function (key, value) {
                $("#ddlDist").append($("<option></option>").val(value.dstCode).html(value.dstName));
            });
            $.each(jsonMaxID, function (key, value) {
                $("#txtAreaID").val(value.MaxFS);
            });

          
        },
        failure: function (response) {
            alert("failure");
        },
        error: function (response) {
            alert("error");
        }
    });
}
function getID()
{
    var action = null;
    action = "BindDistrict";
    $.ajax({
        type: "POST",
        url: "DropDown.aspx/BindDropDownList",
        data: "{ Action: '" + action + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {
            var TableGroupName = data.d;
            var TableArray = TableGroupName.split('^');            
            var jsonMaxID = jQuery.parseJSON(TableArray[1]);            
            $.each(jsonMaxID, function (key, value) {
                $("#txtAreaID").val(value.MaxFS);
            });


        },
        failure: function (response) {
            alert("failure");
        },
        error: function (response) {
            alert("error");
        }
    });
}
function FillGrid() {

    $.ajax({
        type: "POST",
        url: "AreaMaster.aspx/GetTaluka",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {

            var json = JSON.parse(data.d);
            $('#TblSAreaDetail tbody tr').remove();
            $.each(json, function (key, value) {
                //var col1 = '<tr><td onclick="Edit_Row($(this))" class="td width-100 btn btn-block btn-flat center"><i class="ace-icon fa fa-edit blue bigger-150"></i></td>';
                var col1 = '<tr><td onclick="Edit_Row($(this))" class="td width-100 btn btn-block btn-flat center"><i class="ace-icon fa fa-pencil-square-o bigger-150"></i></td>';
                var col2 = '<td class="td code center">' + value.tlkCode + '</td>';
                var col3 = '<td class="td  name left">' + value.tlkName + '</td>';                
                var col4 = '<td class="td  distname center">' + value.dstName + '</td>';                
                var col5 = '<td class="td hide distcode left ">' + value.dstCode + '</td>';
                $("tbody").append(col1 + col2 + col3 + col4 + col5);
            });
            Paging('TblSAreaDetail');
        },
        failure: function (response) {
            alert("failure");
        },
        error: function (response) {
            alert("error");
        }
    });
}

function Edit_Row(row) {
    var ID = row.closest("tr").find(".code").text();
    var Name = row.closest("tr").find(".name").text();
    var dist = row.closest("tr").find(".distcode").text();
    
    $("#Div-Form").show();
    LoadDataToForm(ID, Name, dist);    
}
function BackClick() {    
    $("#Div-Form").addClass("hide")
}
function BtnBackClick()
{
window.location.href="WebForm1.aspx"
}
function BtnNewClick() {
    
    $("#Div-Form").removeClass("hide");
    $("#txtName").focus();
}

function LoadDataToForm(ID, Name, dist)
{
    $("#Div-Form").removeClass("hide")
    $("#btnSave").html('<i class="ace-icon fa fa-floppy-o white"></i>&nbsp;UPDATE');
    $('#btnSave').attr('value', 'UPDATE');
    $("#txtAreaID").val(ID);
    $("#txtName").val(Name);
    $("#ddlDist").val(dist);
}
function btnSave_click() {

    var operation = $('#btnSave').attr('value');

    if (operation == "Save") {

        SaveData("INSERT");
    }
    else if (operation == "UPDATE") {
        SaveData("UPDATE");
    }
}
function AreObjCreate() {
    var _Area = {};
    _Area.tlkCode = 0;
    _Area.tlkName = 0;
    _Area.dstCode = '';
    _Area.dstName = '';    
    _Area.Action = '';

    return _Area;
}
function SaveData(action) {
    if ( $("#txtName").val().trim() == "" || $("#ddlDist").val().trim() == "0") {
        $(".required").blur();
    }
    else {
        var area = AreObjCreate();

        area.tlkCode = parseInt($("#txtAreaID").val().trim());
        area.tlkName = $("#txtName").val().trim();        
        area.dstCode = parseInt($("#ddlDist  option:selected").val());

        area.Action = action;
        $.ajax({
            type: "POST",
            url: "AreaMaster.aspx/InsertUpdateDelete",
            data: JSON.stringify({ 'area': area }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {

                if (area.Action == "UPDATE") {
                    alert("area.Action" + area.Action)
                    $("#Div-Form").addClass("hide");
                    FillGrid();
                    getID();
                    ClearAll();
                }
                else if (area.Action == "INSERT") {
                    ohSnap(data.d);
                    ClearAll()
                    getID();
                    FillGrid();
                }
                
                $("#txtName").focus();               
            },
            failure: function (response) {
                alert(response.d);
            },
            error: function (xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
            }
        });
        
    }
}
function DateFormat(Date) {
    var splitDate = Date.split('/');
    var date = splitDate[2] + "/" + splitDate[1] + "/" + splitDate[0];
    return date;
}
function ClearAll() {
    $("#txtName").val("")
    $("#ddlDist").val("0")
    

}
