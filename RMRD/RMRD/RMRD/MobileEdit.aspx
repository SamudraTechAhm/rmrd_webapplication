﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.Master" AutoEventWireup="true" CodeBehind="MobileEdit.aspx.cs" Inherits="RMRD.MobileEdit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="External-JS/Object.js"></script>
    <link href="assets/css/Common-Style.css" rel="stylesheet" />
    <script src="assets/js/ohsnap.min.js"></script>
    <script src="assets/js/Validation/Validation.js"></script>
    <style>
        input[type=text], textarea, select {
            -webkit-transition: all 0.30s ease-in-out;
            -moz-transition: all 0.30s ease-in-out;
            -o-transition: all 0.30s ease-in-out;
            outline: none;
            padding: 3px 0px 3px 3px;
            margin: 5px 1px 3px 0px;
            border: 2px solid #DDDDDD;
        }

        input[type=text], textarea, select {
            -webkit-transition: all 0.30s ease-in-out;
            -moz-transition: all 0.30s ease-in-out;
            -o-transition: all 0.30s ease-in-out;
            outline: none;
            padding: 3px 0px 3px 3px;
            margin: 5px 1px 3px 0px;
        }

            input[type=text]:focus, textarea:focus, select:focus {
                box-shadow: 0 0 5px rgba(81, 203, 238, 1);
                padding: 3px 0px 3px 3px;
                margin: 5px 1px 3px 0px;
                border: 2px solid red;
            }

        .alert1 {
            padding: 5px;
            margin-bottom: 5px;
            border: 1px solid #eed3d7;
            border-radius: 4px;
            position: absolute;
            bottom: 0px;
            right: 21px;
            /* Each alert has its own width */
            float: right;
            clear: right;
            color: white;
            background-color: rgba(81, 203, 238, 1);
        }

        

        .spinner {
            display: none;
            position: fixed;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background: rgba( 255, 255, 255, .4 ) url('Images/spinner.gif') 50% 50% no-repeat;
            overflow: hidden;
        }
        </style>
    <script type="text/javascript">
        $(document).ready(function () {

            $("#ddlSocietyName").change(function () {
                var value = $('#ddlSocietyName').val();
                $("#txtSocietyID").val(value);
            });

            $("#txtSocietyID").keyup(function () {
                var value = $("#txtSocietyID").val();
                if ($("#ddlSocietyName option[value='" + value + "']").length > 0) {
                    $("#ddlSocietyName").val(value);
                }
                else {
                    $("#ddlSocietyName").val(0);
                }
            });

            var ID = GetParameterValues('id');
            if (ID != null) {
                GetMobileInfoById(ID);
                $('#BtnSave').val('UPDATE');
                $('#BtnSave').text('UPDATE')
            }
            else {
           
                
            }
            $divspinner = $("#divspinner");
            $(document).ajaxStart(function () {
                $divspinner.show();
            });
            $(document).ajaxStop(function () {
                setTimeout($divspinner.hide(), 50000);
            });

        });
        function GetMobileInfoById(ID) {
            $.ajax({
                type: "POST",
                url: "MobileEdit.aspx/GetSocietyDataByID",
                data: '{SocCode: "' + ID + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    var TableGroupName = data.d;
                    var TableArray = TableGroupName.split('^');
                    var JsonSocietyDetails = jQuery.parseJSON(TableArray[0]);
                    var jsonRouteDetails = jQuery.parseJSON(TableArray[1]);
                    

                    $.each(jsonRouteDetails, function (key, value) {
                        $("#ddlSocietyName").append($("<option></option>").val(value.soccode).html(value.socname));
                    });

                    
                    $.each(JsonSocietyDetails, function (key, value) {
                        $("#ddlSocietyName").val(value.soccode);
                        $("#txtSocietyID").val(value.soccode);
                        $("#txtMobileNo").val(value.mobileno);

                    });


                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
        function UpdateData() {
            if ($("#txtSocietyID").val().trim() == "" || $("#ddlSocietyName").val().trim() == "0" || $("#txtMobileNo").val().trim() == "") {
                $(".required").blur();
            }
            else {
                UpdateRecord();
            }
        }
        function SocietyMasterEditObjCreate() {
            var _SocietyMasterEdit = {};
            _SocietyMasterEdit.Row_ID = 0,
            _SocietyMasterEdit.SocCode = 0,
            _SocietyMasterEdit.socName = '',
            _SocietyMasterEdit.Action = '',
            _SocietyMasterEdit.socName2 = '',
            _SocietyMasterEdit.soccodetxt = '',
            _SocietyMasterEdit.rtCode = 0,
            _SocietyMasterEdit.fsCode = 0,
            _SocietyMasterEdit.tlkCode = 0,
            _SocietyMasterEdit.CId = 0,
            _SocietyMasterEdit.mobileno = '',
            _SocietyMasterEdit.IsManual = 2
            return _SocietyMasterEdit;
        }
        function UpdateRecord() {
            var SocietyMasterEdit = SocietyMasterEditObjCreate();
            SocietyMasterEdit.SocCode = $('#txtSocietyID').val();
            SocietyMasterEdit.socName = $('#ddlSocietyName').val();
            SocietyMasterEdit.mobileno = $('#txtMobileNo').val();
            SocietyMasterEdit.Action = "UPDATE";

            $.ajax({
                type: "POST",
                url: "MobileEdit.aspx/UpdateSocietyDataByID",
                data: JSON.stringify({ 'SocietyMasterEdit': SocietyMasterEdit }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    ohSnap(data.d);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
        function BackClick() {
            window.location = "MobileMasterEdit.aspx";
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="breadcrumbs center" id="breadcrumbs">
        <ul class="breadcrumb ">
            <li>
                <%--<i class="ace-icon fa  fa-asterisk"></i>--%>
                <span style="font-family: Verdana; font-size: 20px; color: deepskyblue">MOBILE DETAIL</span>
            </li>

        </ul>

    </div>
    <div class="row">
        <div class="col-lg-2 col-sm-2"></div>
        <div class="col-lg-8 col-sm-8">
            <div class="widget-box widget-color-blue ui-sortable-handle">
                <div class="widget-header">
                    <h5 class="widget-title">MOBILE DETAILS</h5>

                    <div class="widget-toolbar">
                        <a href="#" data-action="collapse">
                            <i class="1 ace-icon fa fa-chevron-up bigger-125"></i>
                        </a>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="widget-main alert alert-info">
                        <div class="row">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-4">
                                <label id="lblEmployeName" class="pull-left">
                                    <h5>Society ID</h5>
                                </label>
                            </div>
                            <div class="col-lg-4">
                                <input type="text" id="txtSocietyID" class=" required Numaric form-control margin-top-4" tabindex="0" data-toggle="SpnSocCode"/>
                            </div>
                           <div class="col-lg-2"></div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6"></div>
                            <div class="col-lg-4">
                                <span class="red margin-bottom-4 input pull-right" id="SpnSocCode" style="display: none;"><i class="ace-icon red bigger-140 fa fa-times-circle"></i>&nbsp;Soc ID Is Required</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2"></div>
                             <div class="col-lg-4">
                                <label id="LblSocietyName" class="pull-left">
                                    <h5>Society Name</h5>
                                </label>
                            </div>
                            <div class="col-lg-4">
                                  <select id="ddlSocietyName" class="form-control required" data-toggle="spnddlSocietyName" tabindex="2">
                                    <option value="0">SELECT</option>
                                </select>
                            </div>
                            <div class="col-lg-2"></div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6"></div>
                            <div class="col-lg-4">
                                <span class="red margin-bottom-4 input pull-right" id="spnddlSocietyName" style="display: none;"><i class="ace-icon red bigger-140 fa fa-times-circle"></i>&nbsp;Society Name Is Required</span>
                            </div>
                        </div>
                                        
                        <div class="row">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-4">
                                <label id="Label10" class="pull-left">
                                    <h5>Mobile No</h5>
                                </label>
                            </div>
                            <div class="col-lg-4">
                                <input type="text" id="txtMobileNo" class=" required Numaric form-control margin-top-4" tabindex="0" data-toggle="SpntxtMobileNo" maxlength="10"/>
                            </div>
                          <div class="col-lg-2"></div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6"></div>
                            <div class="col-lg-4">
                                 <span class="red margin-bottom-4 input pull-right" id="SpntxtMobileNo" style="display: none;"><i class="ace-icon red bigger-140 fa fa-times-circle"></i>&nbsp;Mobile Number Is Required</span>
                            </div>
                        </div>
                        

                    </div>

                    <div class="widget-toolbox padding-8 clearfix">
                        <div class="row">
                            <div class="col-lg-4">

                                <button class="btn btn-xs btn-danger pull-left" type="button" id="btnBack" onclick="BackClick()">
                                    <i class="ace-icon fa fa-arrow-left icon-on-left"></i>
                                    <span class="bigger-110">Back</span>
                                </button>
                            </div>
                            <div class="col-lg-4 col-sm-4">
                                <br />
                                <br />
                                <div id="ohsnap"></div>
                            </div>
                            <div class="col-lg-4">

                                <button class="btn btn-xs btn-info  pull-right" type="button" onclick="UpdateData()" id="BtnSave" value="Save">
                                    <i class="ace-icon fa fa-floppy-o white"></i>
                                    <span class="bigger-110">SAVE</span>

                                </button>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        <div class="col-lg-2 col-sm-2"></div>
    </div>


    <div class="spinner" id="divspinner"></div>

</asp:Content>
