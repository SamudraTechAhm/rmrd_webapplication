﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.Master" AutoEventWireup="true" CodeBehind="MobileMasterEdit.aspx.cs" Inherits="RMRD.MobileMasterEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="External-JS/Paging.js"></script>
    <link href="assets/css/Common-Style.css" rel="stylesheet" />
    <style>
        input[type=text], textarea, select {
            -webkit-transition: all 0.30s ease-in-out;
            -moz-transition: all 0.30s ease-in-out;
            -o-transition: all 0.30s ease-in-out;
            outline: none;
            padding: 3px 0px 3px 3px;
            margin: 5px 1px 3px 0px;
            border: 2px solid #DDDDDD;
        }

            input[type=text]:focus, textarea:focus, select:focus {
                box-shadow: 0 0 5px rgba(81, 203, 238, 1);
                padding: 3px 0px 3px 3px;
                margin: 5px 1px 3px 0px;
                border: 2px solid rgba(81, 203, 238, 1);
            }

        .alert1 {
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid #eed3d7;
            border-radius: 4px;
            position: absolute;
            bottom: 0px;
            right: 21px;
            /* Each alert has its own width */
            float: right;
            clear: right;
            color: white;
            background-color: #DA4453;
        }

        .spinner {
            display: none;
            position: fixed;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background: rgba( 255, 255, 255, .4 ) url('Images/spinner.gif') 50% 50% no-repeat;
            overflow: hidden;
        }
    </style>
    <style>
      
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $divspinner = $("#divspinner");
            $(document).ajaxStart(function () {
                $divspinner.show();
            });
            $(document).ajaxStop(function () {
                setTimeout($divspinner.hide(), 50000);
            });
            FillGrid();
        });
        function FillGrid() {
            $.ajax({
                type: "POST",
                url: "MobileMasterEdit.aspx/GetMobileInformation",
                data: {},
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    var json = JSON.parse(data.d);
                    $.each(json, function (key, value) {
                        var col1 = '<tr><td onclick="Edit_Row($(this))" class="td  btn btn-block btn-flat center"><i class="ace-icon fa fa-pencil-square-o bigger-150"></i></td>';
                        var col2 = '<td class="td code center">' + value.soccode + '</td>';
                        var col3 = '<td class="td  left">' + value.socname + '</td>';
                        var col4 = '<td class="td  center">' + value.mobileno + '</td>';
                       $("tbody").append(col1 + col2 + col3 + col4);
                    });

                    Paging('TblSocietyDetail');
                },
                failure: function (response) {
                    alert("failure");
                },
                error: function (response) {
                    alert("error");
                }
            });
        }

        function Edit_Row(row) {
            var $item = row.closest("tr").find(".code").text();
            var url = "MobileEdit.aspx?id=" + $item;
            window.location = url;
        }
    </script>
    <style>
        .spinner {
            display: none;
            position: fixed;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background: rgba( 255, 255, 255, .4 ) url('Images/spinner.gif') 50% 50% no-repeat;
            overflow: hidden;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="breadcrumbs center" id="breadcrumbs">
        <ul class="breadcrumb ">
            <li>
                <i class="ace-icon fa  fa-asterisk"></i>
                <a href="#" style="font-family: Calibri; font-size: larger">Mobile Number Information</a>
            </li>

        </ul>
    </div>
    <div class="widget-box widget-color-blue ui-sortable-handle">
        <div class="widget-header">
            <h5 class="widget-title">Mobile Number Details</h5>
            <div class="widget-toolbar">
                <a href="#" data-action="collapse">
                    <i class="1 ace-icon fa fa-chevron-up bigger-125"></i>
                </a>
            </div>
            <div class="widget-toolbar no-border">
                <div id="nav-search" class="nav-search">
                    <form class="form-search">
                        <span class="input-icon">
                            <input placeholder="Search..." type="text" id="nav-search-input" class="nav-search-input Search" data-toggle="TblSocietyDetail" autocomplete="off" />

                        </span>
                    </form>
                </div>

            </div>
        </div>
        <div class="widget-body">
            <div class="widget-main ">
                <div class="box-body scroll--200 ">
                    <table id="TblSocietyDetail" class="table">
                        <thead class="bg-blue-light">
                            <tr>
                                <th class="td center"></th>
                                <th class="td center">Society Code</th>
                                <th class="td center">Society Name</th>
                                <th class="td center">Mobile Number</th>
                            </tr>
                        </thead>
                        <tbody class=""></tbody>
                    </table>
                </div>
                <div class="center">
                    <div class="space-4 tbl-footer-bg"></div>
                    <div class="tbl-footer-bg">
                        <button onclick="FirstPage('TblSocietyDetail')" class="margin-bottom-4 margin-top-4 btn btn-sm btn-tumblr" type="button"><i class="ace-icon fa fa-angle-double-left"></i></button>
                        <button onclick="PreviousPage('TblSocietyDetail')" class="margin-bottom-4 margin-top-4 btn btn-sm btn-tumblr" type="button"><i class="ace-icon fa fa-angle-left"></i></button>
                        <b class="bigger-100">|</b>&nbsp;&nbsp;<span dir="ltr">Page<span id="sp_0_grid-pager">1</span>of <span id="sp_1_grid-pager">23</span></span>
                        <b class="bigger-100">|</b>&nbsp;&nbsp;
                                             <button onclick="NextPage('TblSocietyDetail')" class="margin-bottom-4 margin-top-4 btn btn-sm btn-tumblr" type="button"><i class="ace-icon fa fa-angle-right"></i></button>
                        <button onclick="LastPage('TblSocietyDetail')" class="margin-bottom-4 margin-top-4 btn btn-sm btn-tumblr" type="button"><i class="ace-icon fa fa-angle-double-right"></i></button>
                    </div>
                    <div class="space-4 tbl-footer-bg"></div>
                </div>
            </div>

        </div>
    </div>
    <div class="spinner" id="divspinner"></div>
</asp:Content>
