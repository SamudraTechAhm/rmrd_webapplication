﻿using RMRD.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RMRD
{
    public partial class TruckArrivalDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static string GetTruckArrivalInfo()
        {
            BALTruckArriaval objBALTruckArriaval = null;
            try
            {
                objBALTruckArriaval = new BALTruckArriaval();
                return objBALTruckArriaval.BALSelectData("SP_DropDownList", "BindRoutedetails");
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally { objBALTruckArriaval = null; }
        }
    }
}