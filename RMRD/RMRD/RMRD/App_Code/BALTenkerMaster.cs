﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMRD.App_Code
{
    public class BALTenkerMaster
    {
        #region property
            private Int64? _VehicalCode = null;
            private string _VehicalName = null;
            private string _type = null;
            private string _Capacity = null;
            private Int64? _RtcCode = null;
            private Int64? _MobileNo1 = null;    
            private Int64? _CntCode = null;           
            private Int64? _CenterCode = null;
            private Int64? _CId = null;
            private string _CDate = string.Empty;
            private string _Action = string.Empty;

            public Int64? VehicalCode
            {
                get { return _VehicalCode; }
                set { _VehicalCode = value; }
            }
            public string VehicalName
            {
                get { return _VehicalName; }
                set { _VehicalName = value; }
            }
            public string type
            {
                get { return _type; }
                set { _type = value; }
            }
            public string Capacity
            {
                get { return _Capacity; }
                set { _Capacity = value; }
            }
            public Int64? RtcCode
            {
                get { return _RtcCode; }
                set { _RtcCode = value; }
            }
            public Int64? MobileNo1
            {
                get { return _MobileNo1; }
                set { _MobileNo1 = value; }
            }
            public Int64? CntCode
            {
                get { return _CntCode; }
                set { _CntCode = value; }
            }
            public Int64? CenterCode
            {
                get { return _CenterCode; }
                set { _CenterCode = value; }
            }
            public Int64? CId
            {
                get { return _CId; }
                set { _CId = value; }
            }
            public string Action
            {
                get { return _Action; }
                set { _Action = value; }
            }
        #endregion
        public string BALSelectAll(string ProcedureName, Int64 VehicalCode)
        {
            DALTenkerMaster objDALSupervisiorMaster = null;
            try
            {
                objDALSupervisiorMaster = new DALTenkerMaster();
                return objDALSupervisiorMaster.DALSelectVehical(ProcedureName, VehicalCode);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                objDALSupervisiorMaster = null;
            }
        }
        public string BALInsertAndUpdate(string ProcedureName, BALTenkerMaster objBALTenkerMaster = null)
        {
            DALTenkerMaster objDALTenkerMaster = null;
            try
            {
                objDALTenkerMaster = new DALTenkerMaster();
                return objDALTenkerMaster.DALInasertMstLedger(ProcedureName, objBALTenkerMaster);
            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                objDALTenkerMaster = null;
                objBALTenkerMaster = null;
            }
        }
    }
}