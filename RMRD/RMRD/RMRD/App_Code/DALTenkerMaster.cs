﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace RMRD.App_Code
{
    public class DALTenkerMaster
    {
        public string DALSelectVehical(string ProcedureName, Int64 VehicalCode)
        {
            string Action = string.Empty;
            if (VehicalCode == 0)
                Action = "SELECT";
            else
                Action = "SELECTByCode";
            DbHelper objDbHelper = null;
            List<SqlParameter> sqlParam = null;
            try
            {
                objDbHelper = new DbHelper();
                sqlParam = new List<SqlParameter>();

                sqlParam.Add(new SqlParameter("@Action", Action));
                sqlParam.Add(new SqlParameter("@VehicalCode", VehicalCode));
                GetConnectionString objGetConnectionString = new GetConnectionString();
                return objDbHelper.SelecctData(ProcedureName, sqlParam, objGetConnectionString.GetNodeValues("RMRDPlus"));
                //return objDbHelper.SelecctData(ProcedureName, sqlParam, objGetConnectionString.GetNodeValues(HttpContext.Current.Session["YearString"].ToString()));
            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                objDbHelper = null;
                sqlParam = null;
            }
        }
        public string DALInasertMstLedger(string ProcedureName, BALTenkerMaster objBALTenkerMaster = null)
        {
            DbHelper objDbHelper = null;
            List<SqlParameter> sqlParam = null;
            try
            {
                objDbHelper = new DbHelper();
                sqlParam = new List<SqlParameter>();

                sqlParam.Add(new SqlParameter("@VehicalCode", objBALTenkerMaster.VehicalCode));
                sqlParam.Add(new SqlParameter("@VehicalName", objBALTenkerMaster.VehicalName));
                sqlParam.Add(new SqlParameter("@RtcCode", objBALTenkerMaster.RtcCode));
                sqlParam.Add(new SqlParameter("@type", objBALTenkerMaster.type));
                sqlParam.Add(new SqlParameter("@Capacity", objBALTenkerMaster.Capacity));
                sqlParam.Add(new SqlParameter("@MobileNo1", objBALTenkerMaster.MobileNo1));
                
                sqlParam.Add(new SqlParameter("@Action", objBALTenkerMaster.Action));
                sqlParam.Add(new SqlParameter("@CntCode", MainVariable.CntCode));
                sqlParam.Add(new SqlParameter("@CompanyCode", MainVariable.CompanyCode));
                sqlParam.Add(new SqlParameter("@CId", MainVariable.UserID));
                sqlParam.Add(new SqlParameter("@MId", MainVariable.UserID));

                GetConnectionString objGetConnectionString = new GetConnectionString();
                return objDbHelper.SelectInsertAndSelectDelete(ProcedureName, sqlParam, objGetConnectionString.GetNodeValues("RMRDPlus"));
                //return objDbHelper.SelecctData(ProcedureName, sqlParam, objGetConnectionString.GetNodeValues("RMRDPlus"));


            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                objDbHelper = null;
                sqlParam = null;
            }
        }
    }
}