﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace RMRD.App_Code
{
    public class DbHelper
    {
        
        #region Connections
        /// <summary>
        /// Class Name  : DbHelper
        /// Define All Parameter For Connection and SQL Operation
        /// </summary>
        string ConnectionString = ConfigurationManager.ConnectionStrings["LocalConnection"].ConnectionString.ToString();
        SqlConnection con = null;
        SqlCommand cmd = null;
        SqlDataReader sdr = null;
        SqlDataAdapter sda = null;
        DataSet ds = null;
        DataTable dt = null;
        #endregion

       
        #region Insert, Update and Delete in Database
        /// <summary>
        /// Class Name  : DbHelper
        /// Method Name : InsertUpdateDelete
        /// Description : comman method for all Select operation for  InsertUpdateDelete Record for each table.
        /// </summary>
        /// <param name="ProcedureName"></param>
        /// <param name="cParam"></param>
        /// <param name="ConnStrin"></param>
        public void InsertUpdateDelete(string ProcedureName, List<SqlParameter> cParam = null, string ConnString = null)
        {
            try
            {
                if (!string.IsNullOrEmpty(ConnString))
                    con = new SqlConnection(ConnString);
                else if (string.IsNullOrEmpty(ConnString))
                    con = new SqlConnection(ConnectionString);

                //con = new SqlConnection(ConnectionString);
                cmd = new SqlCommand(ProcedureName, con);
                cmd.CommandType = CommandType.StoredProcedure;
                if (cParam != null)
                {
                    for (int i = 0; i < cParam.Count; i++)
                    {
                        cmd.Parameters.Add(cParam[i]);
                    }
                }
                con.Open();
                cmd.CommandTimeout = 0;
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex.GetBaseException();
            }
            finally
            {
                con.Close();
                con.Dispose();
                cmd.Dispose();
            }
        }
        #endregion
        public string SelectInsertAndSelectDelete(string ProcedureName, List<SqlParameter> cParam = null, string ConnString = null)
        {
            string StrReturn = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(ConnString))
                    con = new SqlConnection(ConnString);
                else if (string.IsNullOrEmpty(ConnString))
                    con = new SqlConnection(ConnectionString);

                //con = new SqlConnection(ConnectionString);
                cmd = new SqlCommand(ProcedureName, con);
                cmd.CommandType = CommandType.StoredProcedure;
                if (cParam != null)
                {
                    for (int i = 0; i < cParam.Count; i++)
                    {
                        cmd.Parameters.Add(cParam[i]);
                    }
                }
                con.Open();
                cmd.CommandTimeout = 0;
                SqlDataReader DR = DR = cmd.ExecuteReader();
               
                if (DR.HasRows)
                {
                    string SelectData=string.Empty;
                    while (DR.Read())
                    {
                         SelectData = DR[0].ToString();
                        
                    }
                    StrReturn = SelectData;
                }
             //   cmd.ExecuteNonQuery();
                return StrReturn;
            }
            catch (Exception ex)
            {

                throw ex.GetBaseException();
            }
            finally
            {
                con.Close();
                con.Dispose();
                cmd.Dispose();
            }
        }
       
        #region Select Data
        /// <summary>
        ///  Class Name  : DbHelper
        /// Method Name : SelecctData
        /// Description : comman method for all Select operation for each table for Select row or whole record from table. 
        /// </summary>
        /// <param name="ProcedureName"></param>
        /// <param name="cParam"></param>
        /// <param name="ConnStrin"></param>
        /// <returns></returns>
        public string SelecctData(string ProcedureName, List<SqlParameter> cParam = null, string ConnString = null)
        {
            string StrReturn = string.Empty;
            try
            {
                ds = new DataSet();
                if (!string.IsNullOrEmpty(ConnString))
                    con = new SqlConnection(ConnString);
                else if (string.IsNullOrEmpty(ConnString))
                    con = new SqlConnection(ConnectionString);
                //dt = new DataTable();
                //con = new SqlConnection(ConnectionString);
                cmd = new SqlCommand(ProcedureName, con);
                cmd.CommandType = CommandType.StoredProcedure;
                if (cParam != null)
                {
                    for (int i = 0; i < cParam.Count; i++)
                    {
                        cmd.Parameters.Add(cParam[i]);
                    }
                }
                cmd.CommandTimeout = 0;
                sda = new SqlDataAdapter();
                sda.SelectCommand = cmd;
                sda.Fill(ds);
                if (ds.Tables.Count > 1)
                {
                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        if (i == 0)
                            StrReturn = DataTableToJSONWithStringBuilder(ds.Tables[i]);
                        else
                            StrReturn += "^" + DataTableToJSONWithStringBuilder(ds.Tables[i]);
                    }
                    return StrReturn;//DataTableToJSONWithStringBuilder(ds.Tables[0]) + "^" + DataTableToJSONWithStringBuilder(ds.Tables[1]);
                }
                else if (ds.Tables.Count == 0)
                    return "[]";
                else
                {
                    string JSON = DataTableToJSONWithStringBuilder(ds.Tables[0]);
                    return JSON;
                }
            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                con.Close();
                con.Dispose();
                cmd.Dispose();
            }

        }
        #endregion

       
        #region Selecct Sales Data
        /// <summary>
        /// Class Name  : DbHelper
        /// Method Name : SelecctSales
        /// Description : Select Sales Record
        /// </summary>
        /// <param name="ProcedureName"></param>
        /// <param name="cParam"></param>
        /// <param name="ConnStrin"></param>
        /// <returns></returns>
        public string SelecctSales(string ProcedureName, List<SqlParameter> cParam = null, string ConnString = null)
        {
            try
            {
                ds = new DataSet();
                if (!string.IsNullOrEmpty(ConnString))
                    con = new SqlConnection(ConnString);
                else if (string.IsNullOrEmpty(ConnString))
                    con = new SqlConnection(ConnectionString);

                //con = new SqlConnection(ConnectionString);
                cmd = new SqlCommand(ProcedureName, con);
                cmd.CommandType = CommandType.StoredProcedure;
                if (cParam != null)
                {
                    for (int i = 0; i < cParam.Count; i++)
                    {
                        cmd.Parameters.Add(cParam[i]);
                    }
                }
                cmd.CommandTimeout = 0;
                sda = new SqlDataAdapter();
                sda.SelectCommand = cmd;
                sda.Fill(ds);
                //string JSON;
                //for (int i = 0; i < ds.Tables.Count; i++)
                //{
                //    JSON = DataTableToJSONWithStringBuilder(ds.Tables[i]) + "^";
                //}
                //return JSON;
                string JSON = DataTableToJSONWithStringBuilder(ds.Tables[0]);
                string JSONDetail = DataTableToJSONWithStringBuilder(ds.Tables[1]);
                return JSON + "^" + JSONDetail;
            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                con.Close();
                con.Dispose();
                cmd.Dispose();
            }

        }
        #endregion

       
        #region Select Data for Report
        /// <summary>
        /// Class Name  : DbHelper
        /// Method Name : SelectReportData
        /// Description : Select sale record For Report creation.
        /// </summary>
        /// <param name="ProcedureName"></param>
        /// <param name="cParam"></param>
        /// <param name="ConnStrin"></param>
        /// <returns></returns>
        public DataSet SelectReportData(string ProcedureName, List<SqlParameter> cParam = null, string ConnString = null)
        {
            try
            {
                ds = new DataSet();
                if (!string.IsNullOrEmpty(ConnString))
                    con = new SqlConnection(ConnString);
                else if (string.IsNullOrEmpty(ConnString))
                    con = new SqlConnection(ConnectionString);

                //ds = new DataSet();
                //con = new SqlConnection(ConnString);
                cmd = new SqlCommand(ProcedureName, con);
                cmd.CommandType = CommandType.StoredProcedure;
                if (cParam != null)
                {
                    for (int i = 0; i < cParam.Count; i++)
                    {
                        cmd.Parameters.Add(cParam[i]);
                    }
                }
                cmd.CommandTimeout = 0;
                sda = new SqlDataAdapter();
                sda.SelectCommand = cmd;
                sda.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                con.Close();
                con.Dispose();
                cmd.Dispose();
            }

        }
        #endregion

       
        #region Data Table To JSON With StringBuilder
        /// <summary>
        /// Class Name  : DbHelper
        /// Method Name : DataTableToJSONWithStringBuilder
        /// Description : comman method for all Convert record into Json to Datatable  for each table.
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public string DataTableToJSONWithStringBuilder(DataTable table)
        {
            try
            {
                var JSONString = JsonConvert.SerializeObject(table, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore });
                return JSONString.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.GetBaseException().ToString());
            }

        }
        #endregion

    }
}