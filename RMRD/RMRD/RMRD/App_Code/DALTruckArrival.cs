﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using RMRD.App_Code;
namespace RMRD.App_Code
{
    public class DALTruckArrival
    {
        public string DALSelectData(string ProcedureName,string parametername)
        {

            DbHelper objDbHelper = null;
            List<SqlParameter> sqlParam = null;
            try
            {
                objDbHelper = new DbHelper();
                sqlParam = new List<SqlParameter>();

                sqlParam.Add(new SqlParameter("@Action", parametername));

                GetConnectionString objGetConnectionString = new GetConnectionString();
                return objDbHelper.SelecctData(ProcedureName, sqlParam, objGetConnectionString.GetNodeValues("RMRDPlus"));

            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                objDbHelper = null;
                sqlParam = null;
            }
        }
        public string DALInasertUpdateTruckArrival(string ProcedureName, BALTruckArriaval objTruckArrival = null)
        {
            DbHelper objDbHelper = null;
            List<SqlParameter> sqlParam = null;
            try
            {
                objDbHelper = new DbHelper();
                sqlParam = new List<SqlParameter>();

                sqlParam.Add(new SqlParameter("@rtcode", objTruckArrival.rtCode));
                sqlParam.Add(new SqlParameter("@Dumpdate", MainVariable.DumpDate));

                sqlParam.Add(new SqlParameter("@Shift", MainVariable.shift));
                sqlParam.Add(new SqlParameter("@Action", "SELECTBYID"));
                sqlParam.Add(new SqlParameter("@ARRIVALTIME", objTruckArrival.ArrivalTime));
                sqlParam.Add(new SqlParameter("@TRUCKNO", objTruckArrival.TruckNo));
                sqlParam.Add(new SqlParameter("@CntCode", MainVariable.CntCode));
                sqlParam.Add(new SqlParameter("@CompanyCode", MainVariable.CompanyCode));
                sqlParam.Add(new SqlParameter("@CId", MainVariable.UserID));
                sqlParam.Add(new SqlParameter("@MId", MainVariable.UserID));
                GetConnectionString objGetConnectionString = new GetConnectionString();
                return objDbHelper.SelecctData(ProcedureName, sqlParam, objGetConnectionString.GetNodeValues("RMRDPlus"));



            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                objDbHelper = null;
                sqlParam = null;
            }
        }
    }
}