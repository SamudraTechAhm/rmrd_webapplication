﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace RMRD.App_Code
{
    public class DALDropDownList
    {
        /// <summary>
        /// CLass Name  : DALDropDownList
        /// Method Name : DALSelectDropDownList
        /// Description : Select Record from diffrent table by action and ftech record
        /// </summary>
        /// <param name="Action"></param>
        /// <returns></returns>
        #region Selct DropDownList
        public string DALSelectDropDownList(string Action)
        {

            DbHelper objdbhelper = null;
            List<SqlParameter> sqlParam = null;
            try
            {
                objdbhelper = new DbHelper();
                sqlParam = new List<SqlParameter>();
                sqlParam.Add(new SqlParameter("@Action", Action));
                if (!Action.Split(',')[0].Equals("BindCompanyDdl") && !Action.Split(',')[0].Equals("BindYearDdl"))//&& !ActionSplit.Equals("BindCenterDdl"))
                {
                    GetConnectionString objGetConnectionString = new GetConnectionString();
                    return objdbhelper.SelecctData("SP_DropDownList", sqlParam, objGetConnectionString.GetNodeValues("RMRDPlus"));
                }
                return objdbhelper.SelecctData("SP_DropDownList", sqlParam);
            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                objdbhelper = null;
                sqlParam = null;
            }
        }
        #endregion
    }
}