﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using RMRD.App_Code;

namespace RMRD.App_Code
{
    public class DALMobileMaster
    {
        public string DALSelectData(string ProcedureName)
        {

            DbHelper objDbHelper = null;
            List<SqlParameter> sqlParam = null;
            try
            {
                objDbHelper = new DbHelper();
                sqlParam = new List<SqlParameter>();

                sqlParam.Add(new SqlParameter("@Action", "SELECTALL"));

                GetConnectionString objGetConnectionString = new GetConnectionString();
                return objDbHelper.SelecctData(ProcedureName, sqlParam, objGetConnectionString.GetNodeValues("RMRDPlus"));

            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                objDbHelper = null;
                sqlParam = null;
            }
        }
        public string DALSelectData(string ProcedureName, Int64 SocCode)
        {

            DbHelper objDbHelper = null;
            List<SqlParameter> sqlParam = null;
            try
            {
                objDbHelper = new DbHelper();
                sqlParam = new List<SqlParameter>();

                sqlParam.Add(new SqlParameter("@Action", "SELECTBYID"));
                sqlParam.Add(new SqlParameter("@SocCode", SocCode));

                GetConnectionString objGetConnectionString = new GetConnectionString();
                return objDbHelper.SelecctData(ProcedureName, sqlParam, objGetConnectionString.GetNodeValues("RMRDPlus"));

            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                objDbHelper = null;
                sqlParam = null;
            }
        }
        public string DALInasertUpdateMstLedger(string ProcedureName, BALSocietyMasterEdit objSocietyMasterEdit = null)
        {
            DbHelper objDbHelper = null;
            List<SqlParameter> sqlParam = null;
            try
            {
                objDbHelper = new DbHelper();
                sqlParam = new List<SqlParameter>();
                sqlParam.Add(new SqlParameter("@SocCode", objSocietyMasterEdit.SocCode));
                sqlParam.Add(new SqlParameter("@socName", objSocietyMasterEdit.socName));
                sqlParam.Add(new SqlParameter("@mobileno", objSocietyMasterEdit.mobileno));
                sqlParam.Add(new SqlParameter("@Action", objSocietyMasterEdit.Action));
                GetConnectionString objGetConnectionString = new GetConnectionString();
                return objDbHelper.SelectInsertAndSelectDelete(ProcedureName, sqlParam, objGetConnectionString.GetNodeValues("RMRDPlus"));



            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                objDbHelper = null;
                sqlParam = null;
            }
        }

    }
}