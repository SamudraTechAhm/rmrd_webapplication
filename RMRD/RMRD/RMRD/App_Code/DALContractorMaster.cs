﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace RMRD.App_Code
{
    public class DALContractorMaster
    {
        public string DALSelectContractor(string ProcedureName, Int64 RtcCode)
        {
            string Action = string.Empty;
            if (RtcCode == 0)
                Action = "SELECT";
            else
                Action = "SELECTByCode";
            DbHelper objDbHelper = null;
            List<SqlParameter> sqlParam = null;
            try
            {
                objDbHelper = new DbHelper();
                sqlParam = new List<SqlParameter>();

                sqlParam.Add(new SqlParameter("@Action", Action));
                sqlParam.Add(new SqlParameter("@RtcCode", RtcCode));
                GetConnectionString objGetConnectionString = new GetConnectionString();
                return objDbHelper.SelecctData(ProcedureName, sqlParam, objGetConnectionString.GetNodeValues("RMRDPlus"));
                //return objDbHelper.SelecctData(ProcedureName, sqlParam, objGetConnectionString.GetNodeValues(HttpContext.Current.Session["YearString"].ToString()));
            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                objDbHelper = null;
                sqlParam = null;
            }
        }

        public string DALInasertMstLedger(string ProcedureName, BALContractorMaster objBALContractorMaster = null)
        {
            DbHelper objDbHelper = null;
            List<SqlParameter> sqlParam = null;
            try
            {
                objDbHelper = new DbHelper();
                sqlParam = new List<SqlParameter>();

                sqlParam.Add(new SqlParameter("@RtcCode", objBALContractorMaster.RtcCode));
                sqlParam.Add(new SqlParameter("@RtcName", objBALContractorMaster.RtcName));                
                sqlParam.Add(new SqlParameter("@Mobile", objBALContractorMaster.Mobile));
                sqlParam.Add(new SqlParameter("@EmailId", objBALContractorMaster.EmailId));
                sqlParam.Add(new SqlParameter("@Gender", objBALContractorMaster.Gender));
                sqlParam.Add(new SqlParameter("@BirthDate", Convert.ToDateTime(objBALContractorMaster.BirthDate)));
                sqlParam.Add(new SqlParameter("@Add1", objBALContractorMaster.Add1));
                sqlParam.Add(new SqlParameter("@PINCode", objBALContractorMaster.PINCode));
                sqlParam.Add(new SqlParameter("@City", objBALContractorMaster.City));
                sqlParam.Add(new SqlParameter("@State", objBALContractorMaster.State));
                sqlParam.Add(new SqlParameter("@Country", objBALContractorMaster.Country));
                sqlParam.Add(new SqlParameter("@Action", objBALContractorMaster.Action));
                sqlParam.Add(new SqlParameter("@CntCode", MainVariable.CntCode));
                sqlParam.Add(new SqlParameter("@CompanyCode", MainVariable.CompanyCode));
                sqlParam.Add(new SqlParameter("@CId", MainVariable.UserID));
                sqlParam.Add(new SqlParameter("@MId", MainVariable.UserID));


                GetConnectionString objGetConnectionString = new GetConnectionString();
                return objDbHelper.SelectInsertAndSelectDelete(ProcedureName, sqlParam, objGetConnectionString.GetNodeValues("RMRDPlus"));
                //return objDbHelper.SelecctData(ProcedureName, sqlParam, objGetConnectionString.GetNodeValues("RMRDPlus"));


            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                objDbHelper = null;
                sqlParam = null;
            }
        }
    }
}