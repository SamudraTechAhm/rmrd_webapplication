﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMRD.App_Code
{
    public class BALRouteMaster
    {
        #region Property
        private Int64? _Row_ID = null;
        private Int64? _EffCode = null;
        private Int64? _RtCode = null;
        private string _RtName = string.Empty;
        private string _RtSchTimeM = string.Empty;
        private string _RtSchTimeE = string.Empty;
        private float? _RtKMM = null;
        private float? _RtKME = null;
        private Int64? _IsMixMilk = null;
        private Int64? _VihicalNo = null;
        private Int64? _FieldOfficer = null;
        private Int64? _CntCode = null;
        private string _Action = string.Empty;
        private Int64? _CId = null;
        private string _CDate = string.Empty;
        public Int64? Row_ID
        {
            get { return _Row_ID; }
            set { _Row_ID = value; }
        }
        public Int64? EffCode
        {
            get { return _EffCode; }
            set { _EffCode = value; }
        }


        public Int64? RtCode
        {
            get { return _RtCode; }
            set { _RtCode = value; }
        }
        public string RtName
        {
            get { return _RtName; }
            set { _RtName = value; }
        }
        public string RtSchTimeM
        {
            get { return _RtSchTimeM; }
            set { _RtSchTimeM = value; }
        }
        public string RtSchTimeE
        {
            get { return _RtSchTimeE; }
            set { _RtSchTimeE = value; }
        }
        public float? RtKMM
        {
            get { return _RtKMM; }
            set { _RtKMM = value; }
        }

        public float? RtKME
        {
            get { return _RtKME; }
            set { _RtKME = value; }
        }
        public Int64? IsMixMilk
        {
            get { return _IsMixMilk; }
            set { _IsMixMilk = value; }
        }
        public Int64? VihicalNo
        {
            get { return _VihicalNo; }
            set { _VihicalNo = value; }
        }
        public Int64? FieldOfficer
        {
            get { return _FieldOfficer; }
            set { _FieldOfficer = value; }
        }
        public Int64? CntCode
        {
            get { return _CntCode; }
            set { _CntCode = value; }
        }
        public string Action
        {
            get { return _Action; }
            set { _Action = value; }
        }
        public Int64? CId
        {
            get { return _CId; }
            set { _CId = value; }
        }
        public string CDate
        {
            get { return _CDate; }
            set { _CDate = value; }
        }
        #endregion
        public string BALSelectRouteMaster(string ProcedureName, Int64 RouteCode)
        {
            DALRouteMaster objDALRouteMaster = null;
            try
            {
                objDALRouteMaster = new DALRouteMaster();
                return objDALRouteMaster.DALSelectRoute(ProcedureName, RouteCode);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                objDALRouteMaster = null;
            }
        }
        public string BALSelectRouteDetail(string ProcedureName, Int64 RouteCode)
        {
            DALRouteMaster objDALRouteMaster = null;
            try
            {
                objDALRouteMaster = new DALRouteMaster();
                return objDALRouteMaster.DALSelectRouteCode(ProcedureName, RouteCode);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                objDALRouteMaster = null;
            }
        }
        public string BALSelectData(string ProcedureName)
        {
            DALRouteMaster objDALRouteMaster = null;
            try
            {
                objDALRouteMaster = new DALRouteMaster();
                return objDALRouteMaster.DALSelectData(ProcedureName);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                objDALRouteMaster = null;
            }
        }

        public void BALInsertAndUpdate(string ProcedureName, BALRouteMaster objBALRouteMaster = null)
        {
            DALRouteMaster objDALRouteMaster = null;
            try
            {
                objDALRouteMaster = new DALRouteMaster();
                objDALRouteMaster.DALInasertMstLedger(ProcedureName, objBALRouteMaster);
            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                objDALRouteMaster = null;
                objBALRouteMaster = null;
            }
        }
    }
}