﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMRD.App_Code
{
    public class BALMobileMaster
    {
        public string BALSelectData(string ProcedureName)
        {
            DALMobileMaster objMobileMaster = null;
            try
            {
                objMobileMaster = new DALMobileMaster();
                return objMobileMaster.DALSelectData(ProcedureName);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                objMobileMaster = null;
            }
        }
        public string BALSelectData(string ProcedureName, Int64 SocCode)
        {
            DALMobileMaster objMobileMaster = null;
            try
            {
                objMobileMaster = new DALMobileMaster();
                return objMobileMaster.DALSelectData(ProcedureName, SocCode);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                objMobileMaster = null;
            }
        }
        public string BALUpdateData(string ProcedureName, BALSocietyMasterEdit SocietyMasterEdit)
        {
            DALMobileMaster objSocietyMasterEdit = null;
            try
            {
                objSocietyMasterEdit = new DALMobileMaster();
                return objSocietyMasterEdit.DALInasertUpdateMstLedger(ProcedureName, SocietyMasterEdit);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                objSocietyMasterEdit = null;
            }
        }
    }
}