﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using RMRD.App_Code;

namespace RMRD.App_Code
{
    public class ErrorInfo
    {
        [WebMethod]
        public static void InsertData(string ExceptionType, string StackTrace, string Message)
        {
            SqlConnection con=null;
            SqlCommand cmd=null;
            GetConnectionString objGetConnectionString = new GetConnectionString();
            try
            {
                con= new SqlConnection(objGetConnectionString.GetNodeValues("MstCFAS"));
                cmd = new SqlCommand("Sp_LogException", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ExceptionMessage", Message);
                cmd.Parameters.AddWithValue("@Source", ExceptionType);
                cmd.Parameters.AddWithValue("@Trace", StackTrace);
                con.Open();
                cmd.ExecuteNonQuery();
               // return "Error";
            }
            catch (System.Exception)
            {
                
                throw;
            }
            finally
            {
                con.Close();
                con.Dispose();
                cmd.Dispose();
            }
            
        }
    }
}