﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMRD.App_Code
{
    public class BALSocietyMasterEdit
    {
        private Int64? _Row_ID = null;
        private Int64? _SocCode = null;
        private string _socName = "";
        private string _Action = "";
        private string _socName2 = "";
        private string _soccodetxt = "";
        private Int64? _rtCode = null;
        private Int64? _fsCode = null;
        private Int64? _tlkCode = null;
        private Int64? _CId = null;
        private DateTime _CDate;
        private Int64? _MId = null;
        private DateTime _MDate;
        private string _sadacan = "";
        private byte _isUpload = 0;
        private Int64? _cntcode = null;
        private string _mobileno = "";
        private string _roughtname = "";
        private Int64? _IsManual = null;
        
        public Int64? Row_ID
        {
            get { return _Row_ID; }
            set { _Row_ID = value; }
        }
        public Int64? SocCode
        {
            get { return _SocCode; }
            set { _SocCode = value; }
        }
        public string socName
        {
            get { return _socName; }
            set { _socName = value; }
        }
        public string socName2
        {
            get { return _socName2; }
            set { _socName2 = value; }
        }
        public string soccodetxt
        {
            get { return _soccodetxt; }
            set { _soccodetxt = value; }
        }
        public Int64? rtCode
        {
            get { return _rtCode; }
            set { _rtCode = value; }
        }
        public Int64? fsCode
        {
            get { return _fsCode; }
            set { _fsCode = value; }
        }
        public Int64? tlkCode
        {
            get { return _tlkCode; }
            set { _tlkCode = value; }
        }
        public Int64? CId
        {
            get { return _CId; }
            set { _CId = value; }
        }
        public DateTime CDate
        {
            get { return _CDate; }
            set { _CDate = value; }
        }
        public Int64? MId
        {
            get { return _MId; }
            set { _MId = value; }
        }
        public DateTime MDate
        {
            get { return _MDate; }
            set { _MDate = value; }
        }
        public string sadacan
        {
            get { return _sadacan; }
            set { _sadacan = value; }
        }
        public Int64? IsManual
        {
            get { return _IsManual; }
            set { _IsManual = value; }
        }
        public byte isUpload
        {
            get { return _isUpload; }
            set { _isUpload = value; }
        }
        public Int64? cntcode
        {
            get { return _cntcode; }
            set { _cntcode = value; }
        }
        public string mobileno
        {
            get { return _mobileno; }
            set { _mobileno = value; }
        }
        public string Action
        {
            get { return _Action; }
            set { _Action = value; }
        }
        public string roughtname
        {
            get { return _roughtname; }
            set { _roughtname = value; }
        }
        public string BALSelectData(string ProcedureName)
        {
            DALSocietyMasterEdit objSocietyMasterEdit = null;
            try
            {
                objSocietyMasterEdit = new DALSocietyMasterEdit();
                return objSocietyMasterEdit.DALSelectData(ProcedureName);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                objSocietyMasterEdit = null;
            }
        }
        public string BALSelectData(string ProcedureName,Int64 SocCode)
        {
            DALSocietyMasterEdit objSocietyMasterEdit = null;
            try
            {
                objSocietyMasterEdit = new DALSocietyMasterEdit();
                return objSocietyMasterEdit.DALSelectData(ProcedureName,SocCode);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                objSocietyMasterEdit = null;
            }
        }

        public string BALUpdateData(string ProcedureName, BALSocietyMasterEdit SocietyMasterEdit)
        {
            DALSocietyMasterEdit objSocietyMasterEdit = null;
            try
            {
                objSocietyMasterEdit = new DALSocietyMasterEdit();
                return objSocietyMasterEdit.DALInasertUpdateMstLedger(ProcedureName, SocietyMasterEdit);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                objSocietyMasterEdit = null;
            }
        }
    }
}