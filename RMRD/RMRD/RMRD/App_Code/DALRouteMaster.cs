﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using RMRD.App_Code;
namespace RMRD.App_Code
{
    public class DALRouteMaster
    {
        public string DALSelectRoute(string ProcedureName, Int64 RouteCode)
        {
            string Action = string.Empty;
            if (RouteCode == 0)
                Action = "SELECT";
            else
                Action = "SELECTByRouteDate";
            DbHelper objDbHelper = null;
            List<SqlParameter> sqlParam = null;
            try
            {
                objDbHelper = new DbHelper();
                sqlParam = new List<SqlParameter>();

                sqlParam.Add(new SqlParameter("@Action", Action));
                sqlParam.Add(new SqlParameter("@EffCode", RouteCode));
                GetConnectionString objGetConnectionString = new GetConnectionString();
                return objDbHelper.SelecctData(ProcedureName, sqlParam, objGetConnectionString.GetNodeValues("RMRDPlus"));
                //return objDbHelper.SelecctData(ProcedureName, sqlParam, objGetConnectionString.GetNodeValues(HttpContext.Current.Session["YearString"].ToString()));
            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                objDbHelper = null;
                sqlParam = null;
            }
        }
        public string DALSelectRouteCode(string ProcedureName, Int64 RouteCode)
        {

            DbHelper objDbHelper = null;
            List<SqlParameter> sqlParam = null;
            try
            {
                objDbHelper = new DbHelper();
                sqlParam = new List<SqlParameter>();

                sqlParam.Add(new SqlParameter("@Action", "SELECTByRouteCode"));
                sqlParam.Add(new SqlParameter("@RtCode", RouteCode));
                GetConnectionString objGetConnectionString = new GetConnectionString();
                return objDbHelper.SelecctData(ProcedureName, sqlParam, objGetConnectionString.GetNodeValues("RMRDPlus"));
                //return objDbHelper.SelecctData(ProcedureName, sqlParam, objGetConnectionString.GetNodeValues(HttpContext.Current.Session["YearString"].ToString()));
            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                objDbHelper = null;
                sqlParam = null;
            }
        }
        public string DALSelectData(string ProcedureName)
        {

            DbHelper objDbHelper = null;
            List<SqlParameter> sqlParam = null;
            try
            {
                objDbHelper = new DbHelper();
                sqlParam = new List<SqlParameter>();

                sqlParam.Add(new SqlParameter("@Action", "SELECTDATA"));

                GetConnectionString objGetConnectionString = new GetConnectionString();
                return objDbHelper.SelecctData(ProcedureName, sqlParam, objGetConnectionString.GetNodeValues("RMRDPlus"));
                //return objDbHelper.SelecctData(ProcedureName, sqlParam, objGetConnectionString.GetNodeValues(HttpContext.Current.Session["YearString"].ToString()));
            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                objDbHelper = null;
                sqlParam = null;
            }
        }

        public void DALInasertMstLedger(string ProcedureName, BALRouteMaster objBALRouteMaster = null)
        {
            DbHelper objDbHelper = null;
            List<SqlParameter> sqlParam = null;
            try
            {
                objDbHelper = new DbHelper();
                sqlParam = new List<SqlParameter>();

                sqlParam.Add(new SqlParameter("@EffCode", objBALRouteMaster.EffCode));
                sqlParam.Add(new SqlParameter("@RtCode", objBALRouteMaster.RtCode));
                sqlParam.Add(new SqlParameter("@RtName", objBALRouteMaster.RtName));
                sqlParam.Add(new SqlParameter("@RtSchTimeM", Convert.ToDateTime(objBALRouteMaster.RtSchTimeM)));
                sqlParam.Add(new SqlParameter("@RtSchTimeE", Convert.ToDateTime(objBALRouteMaster.RtSchTimeE)));
                sqlParam.Add(new SqlParameter("@RtKMM", objBALRouteMaster.RtKMM));
                sqlParam.Add(new SqlParameter("@RtKME", objBALRouteMaster.RtKME));
                sqlParam.Add(new SqlParameter("@IsMixMilk", objBALRouteMaster.IsMixMilk));
                sqlParam.Add(new SqlParameter("@VihicalNo", objBALRouteMaster.VihicalNo));
                sqlParam.Add(new SqlParameter("@FieldOfficer", objBALRouteMaster.FieldOfficer));
                sqlParam.Add(new SqlParameter("@CntCode", MainVariable.CntCode));
                sqlParam.Add(new SqlParameter("@CompanyCode", MainVariable.CompanyCode));
                sqlParam.Add(new SqlParameter("@CId", MainVariable.UserID));
                sqlParam.Add(new SqlParameter("@MId", MainVariable.UserID));

                sqlParam.Add(new SqlParameter("@Action", objBALRouteMaster.Action));
                GetConnectionString objGetConnectionString = new GetConnectionString();
                objDbHelper.InsertUpdateDelete(ProcedureName, sqlParam, objGetConnectionString.GetNodeValues("RMRDPlus"));
                //return objDbHelper.SelecctData(ProcedureName, sqlParam, objGetConnectionString.GetNodeValues("RMRDPlus"));


            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                objDbHelper = null;
                sqlParam = null;
            }
        }
    }
}