﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using RMRD.App_Code;

namespace RMRD.App_Code
{
    public class BALTruckArriaval
    {
        private Int64? _rtCode=null;
        private DateTime? _DumpDate;
        private string _Shift="";
        private DateTime? _ArrivalTime;
        private string _TruckNo="";
        private DateTime? _SchTime;
        private DateTime?  _ArrivalDelay;
        private string _ArrivalTimeTxt="";
        private string _SchTimeTxt="";
        private Int64? _CId=null;
        private DateTime? _CDate;
        private Int64? _MId=null;
       
        public Int64? rtCode
        {
            get { return _rtCode; }
            set { _rtCode = value; }
        }
        public DateTime? DumpDate
        {
            get { return _DumpDate; }
            set { _DumpDate = value; }
        }
        public string Shift
        {
            get { return _Shift; }
            set { _Shift = value; }
        }
        public DateTime? ArrivalTime
        {
            get { return _ArrivalTime; }
            set { _ArrivalTime = value; }
        }
        public string TruckNo
        {
            get { return _TruckNo; }
            set { _TruckNo = value; }
        }
        public DateTime? SchTime
        {
            get { return _SchTime; }
            set { _SchTime = value; }
        }
        public DateTime? ArrivalDelay
        {
            get { return _ArrivalDelay; }
            set { _ArrivalDelay = value; }
        }
        public string ArrivalTimeTxt
        {
            get { return _ArrivalTimeTxt; }
            set { _ArrivalTimeTxt = value; }
        }
        public string SchTimeTxt
        {
            get { return _SchTimeTxt; }
            set { _SchTimeTxt = value; }
        }
        public Int64? CId
        {
            get { return _CId; }
            set { _CId = value; }
        }
        public DateTime? CDate
        {
            get { return _CDate; }
            set { _CDate = value; }
        }
        public Int64? MId
        {
            get { return _MId; }
            set { _MId = value; }
        }
        public string BALSelectData(string ProcedureName,String parametername)
        {
            DALTruckArrival objTruckArrival = null;
            try
            {
                objTruckArrival = new DALTruckArrival();
                return objTruckArrival.DALSelectData(ProcedureName, parametername);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                objTruckArrival = null;
            }
        }
        public string BALUpdateData(string ProcedureName, BALTruckArriaval SocietyMasterEdit)
        {
            DALTruckArrival objTruckArrival = null;
            try
            {
                objTruckArrival = new DALTruckArrival();
                return objTruckArrival.DALInasertUpdateTruckArrival(ProcedureName, SocietyMasterEdit);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                objTruckArrival = null;
            }
        }
    }
}