﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMRD.App_Code
{
    public class BALSupervisiorMaster
    {
        #region Property
        private Int64? _FsCode = null;
        private string _FsName = string.Empty;
        private string _Designation = string.Empty;
        private Int64? _Mobile = null;
        private string _EmailId = string.Empty;
        private string _Gender = string.Empty;
        private string _BirthDate = string.Empty;
        private string _Add1 = string.Empty;
        private Int64? _PINCode = null;
        private string _City = string.Empty;
        private string _State = string.Empty;
        private string _Country = string.Empty;
        private Int64? _CntCode = null;
        private Int64? _CenterCode = null;
        private Int64? _CId = null;
        private string _CDate = string.Empty;
        private string _Action = string.Empty;


        public Int64? FsCode
        {
            get { return _FsCode; }
            set { _FsCode = value; }
        }
        public string FsName
        {
            get { return _FsName; }
            set { _FsName = value; }
        }
        public string Designation
        {
            get { return _Designation; }
            set { _Designation = value; }
        }
        public Int64? Mobile
        {
            get { return _Mobile; }
            set { _Mobile = value; }
        }
        public string EmailId
        {
            get { return _EmailId; }
            set { _EmailId = value; }
        }
        public string Gender
        {
            get { return _Gender; }
            set { _Gender = value; }
        }
        public string BirthDate
        {
            get { return _BirthDate; }
            set { _BirthDate = value; }
        }
        public string Add1
        {
            get { return _Add1; }
            set { _Add1 = value; }
        }
        public string City
        {
            get { return _City; }
            set { _City = value; }
        }
        public Int64? PINCode
        {
            get { return _PINCode; }
            set { _PINCode = value; }
        }
        public string State
        {
            get { return _State; }
            set { _State = value; }
        }
        public string Country
        {
            get { return _Country; }
            set { _Country = value; }
        }
        public string CDate
        {
            get { return _CDate; }
            set { _CDate = value; }
        }
        public Int64? CntCode
        {
            get { return _CntCode; }
            set { _CntCode = value; }
        }
        public Int64? CenterCode
        {
            get { return _CenterCode; }
            set { _CenterCode = value; }
        }
        public string Action
        {
            get { return _Action; }
            set { _Action = value; }
        }
        #endregion
        public string BALSelectAllSuperVisiour(string ProcedureName,Int64 FsCode)
        {
            DALSupervisiorMaster objDALSupervisiorMaster = null;
            try
            {
                objDALSupervisiorMaster = new DALSupervisiorMaster();
                return objDALSupervisiorMaster.DALSelectSuperVisior(ProcedureName, FsCode);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                objDALSupervisiorMaster = null;
            }
        }

        public string BALInsertAndUpdate(string ProcedureName, BALSupervisiorMaster objBALSupervisiorMaster = null)
        {
            DALSupervisiorMaster objDALSupervisiorMaster = null;
            try
            {
                objDALSupervisiorMaster = new DALSupervisiorMaster();
                return objDALSupervisiorMaster.DALInasertMstLedger(ProcedureName, objBALSupervisiorMaster);
            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                objDALSupervisiorMaster = null;
                objBALSupervisiorMaster = null;
            }
        }
    }
}