﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using RMRD.App_Code;

namespace RMRD.App_Code
{
    public class DALSocietySetting
    {
        public void DALUpdateMstSoceitySetting(string ProcedureName, BALSocietySetting objBALSoceitySetting = null)
        {
            DbHelper objDbHelper = null;
            List<SqlParameter> sqlParam = null;
            try
            {
                objDbHelper = new DbHelper();
                sqlParam = new List<SqlParameter>();

                sqlParam.Add(new SqlParameter("@IsManual", objBALSoceitySetting.IsManual));
                sqlParam.Add(new SqlParameter("@IsAutomatic", objBALSoceitySetting.IsAutomatic));
                GetConnectionString objGetConnectionString = new GetConnectionString();
                objDbHelper.InsertUpdateDelete(ProcedureName, sqlParam, objGetConnectionString.GetNodeValues("RMRDPlus"));
                


            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                objDbHelper = null;
                sqlParam = null;
            }
        }
        public string DALSelectData(string ProcedureName)
        {
            DbHelper objDbHelper = null;
            List<SqlParameter> sqlParam = null;
            try
            {
                objDbHelper = new DbHelper();
                sqlParam = new List<SqlParameter>();
                sqlParam.Add(new SqlParameter("@Action", "SELECT"));
                GetConnectionString objGetConnectionString = new GetConnectionString();
                return objDbHelper.SelecctData(ProcedureName, sqlParam, objGetConnectionString.GetNodeValues("RMRDPlus"));
             
            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                objDbHelper = null;
                sqlParam = null;
            }
        }
    }
}