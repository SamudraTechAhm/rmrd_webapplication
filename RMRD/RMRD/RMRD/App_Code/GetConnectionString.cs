﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace RMRD.App_Code
{
    public class GetConnectionString
    {
        #region Variable
        string servername = string.Empty;
        string username = string.Empty;
        string password = string.Empty;
        string database = string.Empty;
        XmlDocument xmlDoc = null;
        #endregion

        #region Get Connection String from XML
        public string GetNodeValues(string DbName, string IsReport = null)
        {
            string ConnectionString = string.Empty;
            try
            {
                string path = HttpContext.Current.Request.MapPath("~/XML.xml");
                xmlDoc = new XmlDocument();
                xmlDoc.Load(path);
                XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes("/configuration/" + "sam" + DbName);
                if (nodeList.Count == 1)
                {
                    foreach (XmlNode node in nodeList)
                    {
                        servername = node.SelectSingleNode("servername").InnerText;
                        username = node.SelectSingleNode("username").InnerText;
                        password = node.SelectSingleNode("password").InnerText;
                        database = node.SelectSingleNode("database").InnerText;
                    }
                }
                else
                {
                    XmlElement elmRoot = xmlDoc.DocumentElement;
                    servername = elmRoot.Attributes["servername"].Value;
                    username = elmRoot.Attributes["username"].Value;
                    password = elmRoot.Attributes["password"].Value;
                    database = DbName;

                    XmlElement elm = xmlDoc.CreateElement("sam" + DbName);
                    elmRoot.AppendChild(elm);
                    XmlElement elm1 = xmlDoc.CreateElement("servername");
                    elm1.InnerText = servername;
                    XmlElement elm2 = xmlDoc.CreateElement("username");
                    elm2.InnerText = username;
                    XmlElement elm3 = xmlDoc.CreateElement("password");
                    elm3.InnerText = password;
                    XmlElement elm4 = xmlDoc.CreateElement("database");
                    elm4.InnerText = DbName;
                    elm.AppendChild(elm1);
                    elm.AppendChild(elm2);
                    elm.AppendChild(elm3);
                    elm.AppendChild(elm4);
                    xmlDoc.Save(path);
                }
                if (string.IsNullOrEmpty(IsReport))
                    ConnectionString = String.Format("Data Source = {0}; database = {1}; user = {2}; password = {3};", servername, database, username, password);
                else if (!string.IsNullOrEmpty(IsReport) && IsReport.Equals("ReportCredentials"))
                    ConnectionString = servername + "^" + database + "^" + username + "^" + password;
                return ConnectionString;
            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
        }
        #endregion
    }
}