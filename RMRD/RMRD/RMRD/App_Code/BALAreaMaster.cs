﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMRD.App_Code
{
    public class BALAreaMaster
    {
        #region Property
        private Int64? _tlkCode = null;
        private string _tlkName = string.Empty;
        private Int64? _CntCode = null;
        private Int64? _dstCode = null;
        private Int64? _CenterCode = null;
        private Int64? _CId = null;
        private string _CDate = string.Empty;
        private string _Action = string.Empty;

        //GET-SET
        public Int64? tlkCode
        {
            get { return _tlkCode; }
            set { _tlkCode = value; }
        }
        public string tlkName
        {
            get { return _tlkName; }
            set { _tlkName = value; }
        }
        public Int64? dstCode
        {
            get { return _dstCode; }
            set { _dstCode = value; }
        }
        public string CDate
        {
            get { return _CDate; }
            set { _CDate = value; }
        }
        public Int64? CntCode
        {
            get { return _CntCode; }
            set { _CntCode = value; }
        }
        public Int64? CenterCode
        {
            get { return _CenterCode; }
            set { _CenterCode = value; }
        }
        public Int64? CId
        {
            get { return _CId; }
            set { _CId = value; }
        }
        public string Action
        {
            get { return _Action; }
            set { _Action = value; }
        }

        #endregion
        public string BALSelectAllTaluka(string ProcedureName, Int64 tlkCode)
        {
            DALAreaMaster objDALSupervisiorMaster = null;
            try
            {
                objDALSupervisiorMaster = new DALAreaMaster();
                return objDALSupervisiorMaster.DALSelectArea(ProcedureName, tlkCode);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                objDALSupervisiorMaster = null;
            }
        }
        public string BALInsertAndUpdate(string ProcedureName, BALAreaMaster objBALAreaMaster = null)
        {
            DALAreaMaster objDALAreaMaster = null;
            try
            {
                objDALAreaMaster = new DALAreaMaster();
                return objDALAreaMaster.DALInasertMstLedger(ProcedureName, objBALAreaMaster);
            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                objDALAreaMaster = null;
                objBALAreaMaster = null;
            }
        }
    }
}