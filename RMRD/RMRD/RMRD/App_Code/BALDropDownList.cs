﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMRD.App_Code
{
    public class BALDropDownList
    {
        /// <summary>
        /// BALDropDownList
        /// Property Of DropDownLiast
        /// Get, SET Methos For Each Variable
        /// </summary>
        #region Properety
        private Int64? _Value = null;
        private string _Text = string.Empty;

        #region GET SET Mathod
        public Int64? Value
        {
            get { return _Value; }
            set { _Value = value; }
        }
        public string Text
        {
            get { return _Text; }
            set { _Text = value; }
        }
        #endregion
        #endregion

        /// <summary>
        /// BALDropDownList
        /// Method Name : BALSelectDropDownList
        /// Description :Select Record in DropdownList from diffrent Table through "Action" wise
        /// </summary>
        /// <param name="Action"></param>
        /// <returns></returns>
        #region Selct DropDownList
        public string BALSelectDropDownList(string Action)
        {
            DALDropDownList objdaldropdown = null;
            try
            {
                objdaldropdown = new DALDropDownList();
                return objdaldropdown.DALSelectDropDownList(Action);

            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();

            }
            finally
            {
                objdaldropdown = null;
            }
        }
        #endregion
    }
}