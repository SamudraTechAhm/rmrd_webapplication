﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMRD.App_Code
{
    public class BALSocietySetting
    {
        private byte _IsManual = 0;
        private byte _IsAutomatic = 0;
        public byte IsManual
        {
            get { return _IsManual; }
            set { _IsManual = value; }
        }
        public byte IsAutomatic
        {
            get { return _IsAutomatic; }
            set { _IsAutomatic = value; }
        }
        public void BALInsertAndUpdate(string ProcedureName, BALSocietySetting objBALSocietySetting = null)
        {
            DALSocietySetting objDALSocietySetting = null;
            try
            {
                objDALSocietySetting = new DALSocietySetting();
                objDALSocietySetting.DALUpdateMstSoceitySetting(ProcedureName, objBALSocietySetting);
            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                objDALSocietySetting = null;
                objDALSocietySetting = null;
            }
        }
        public string BALSelectData(string ProcedureName)
        {
            DALSocietySetting objSocietySetting = null;
            try
            {
                objSocietySetting = new DALSocietySetting();
                return objSocietySetting.DALSelectData(ProcedureName);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                objSocietySetting = null;
            }
        }
    }
}