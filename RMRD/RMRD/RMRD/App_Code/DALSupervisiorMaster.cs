﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace RMRD.App_Code
{
    public class DALSupervisiorMaster
    {
        public string DALSelectSuperVisior(string ProcedureName, Int64 FsCode)
        {
            string Action = string.Empty;
            if (FsCode == 0)
                Action = "SELECT";
            else
                Action = "SELECTByCode";
            DbHelper objDbHelper = null;
            List<SqlParameter> sqlParam = null;
            try
            {
                objDbHelper = new DbHelper();
                sqlParam = new List<SqlParameter>();

                sqlParam.Add(new SqlParameter("@Action", Action));
                sqlParam.Add(new SqlParameter("@FsCode", FsCode));
                GetConnectionString objGetConnectionString = new GetConnectionString();
                return objDbHelper.SelecctData(ProcedureName, sqlParam, objGetConnectionString.GetNodeValues("RMRDPlus"));
                //return objDbHelper.SelecctData(ProcedureName, sqlParam, objGetConnectionString.GetNodeValues(HttpContext.Current.Session["YearString"].ToString()));
            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                objDbHelper = null;
                sqlParam = null;
            }
        }

        public  string DALInasertMstLedger(string ProcedureName, BALSupervisiorMaster objBALSupervisiorMaster = null)
        {
            DbHelper objDbHelper = null;
            List<SqlParameter> sqlParam = null;
            try
            {
                objDbHelper = new DbHelper();
                sqlParam = new List<SqlParameter>();

                sqlParam.Add(new SqlParameter("@FsCode", objBALSupervisiorMaster.FsCode));
                sqlParam.Add(new SqlParameter("@FsName", objBALSupervisiorMaster.FsName));
                sqlParam.Add(new SqlParameter("@Designation", objBALSupervisiorMaster.Designation));
                sqlParam.Add(new SqlParameter("@Mobile", objBALSupervisiorMaster.Mobile));
                sqlParam.Add(new SqlParameter("@EmailId", objBALSupervisiorMaster.EmailId));
                sqlParam.Add(new SqlParameter("@Gender", objBALSupervisiorMaster.Gender));
                sqlParam.Add(new SqlParameter("@BirthDate", Convert.ToDateTime(objBALSupervisiorMaster.BirthDate)));
                sqlParam.Add(new SqlParameter("@Add1", objBALSupervisiorMaster.Add1));
                sqlParam.Add(new SqlParameter("@PINCode", objBALSupervisiorMaster.PINCode));
                sqlParam.Add(new SqlParameter("@City", objBALSupervisiorMaster.City));
                sqlParam.Add(new SqlParameter("@State", objBALSupervisiorMaster.State));
                sqlParam.Add(new SqlParameter("@Country", objBALSupervisiorMaster.Country));
                sqlParam.Add(new SqlParameter("@Action", objBALSupervisiorMaster.Action));
                sqlParam.Add(new SqlParameter("@CntCode", MainVariable.CntCode));
                sqlParam.Add(new SqlParameter("@CompanyCode", MainVariable.CompanyCode));
                sqlParam.Add(new SqlParameter("@CId", MainVariable.UserID));
                sqlParam.Add(new SqlParameter("@MId", MainVariable.UserID));

                
                GetConnectionString objGetConnectionString = new GetConnectionString();
                return objDbHelper.SelectInsertAndSelectDelete(ProcedureName, sqlParam, objGetConnectionString.GetNodeValues("RMRDPlus"));
                //return objDbHelper.SelecctData(ProcedureName, sqlParam, objGetConnectionString.GetNodeValues("RMRDPlus"));


            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                objDbHelper = null;
                sqlParam = null;
            }
        }
    }
}