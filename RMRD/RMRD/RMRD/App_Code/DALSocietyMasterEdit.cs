﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using RMRD.App_Code;

namespace RMRD.App_Code
{
    public class DALSocietyMasterEdit
    {
        public string DALSelectData(string ProcedureName)
        {

            DbHelper objDbHelper = null;
            List<SqlParameter> sqlParam = null;
            try
            {
                objDbHelper = new DbHelper();
                sqlParam = new List<SqlParameter>();

                sqlParam.Add(new SqlParameter("@Action", "CheckManualAutoConfig"));

                GetConnectionString objGetConnectionString = new GetConnectionString();
                return objDbHelper.SelecctData(ProcedureName, sqlParam, objGetConnectionString.GetNodeValues("RMRDPlus"));
                
            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                objDbHelper = null;
                sqlParam = null;
            }
        }
        public string DALSelectData(string ProcedureName,Int64 SocCode)
        {

            DbHelper objDbHelper = null;
            List<SqlParameter> sqlParam = null;
            try
            {
                objDbHelper = new DbHelper();
                sqlParam = new List<SqlParameter>();

                sqlParam.Add(new SqlParameter("@Action", "GetSocietyDataByID"));
                sqlParam.Add(new SqlParameter("@SocCode", SocCode));

                GetConnectionString objGetConnectionString = new GetConnectionString();
                return objDbHelper.SelecctData(ProcedureName, sqlParam, objGetConnectionString.GetNodeValues("RMRDPlus"));

            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                objDbHelper = null;
                sqlParam = null;
            }
        }
        public string DALInasertUpdateMstLedger(string ProcedureName, BALSocietyMasterEdit objSocietyMasterEdit = null)
        {
            DbHelper objDbHelper = null;
            List<SqlParameter> sqlParam = null;
            try
            {
                objDbHelper = new DbHelper();
                sqlParam = new List<SqlParameter>();

                sqlParam.Add(new SqlParameter("@SocCode", objSocietyMasterEdit.SocCode));
                sqlParam.Add(new SqlParameter("@socName", objSocietyMasterEdit.socName));
                sqlParam.Add(new SqlParameter("@rtCode", objSocietyMasterEdit.rtCode));
                sqlParam.Add(new SqlParameter("@tlkCode", objSocietyMasterEdit.tlkCode));
                sqlParam.Add(new SqlParameter("@mobileno", objSocietyMasterEdit.mobileno));
                sqlParam.Add(new SqlParameter("@Action", objSocietyMasterEdit.Action));
                sqlParam.Add(new SqlParameter("@IsManual", objSocietyMasterEdit.IsManual));
                sqlParam.Add(new SqlParameter("@CntCode", MainVariable.CntCode));
                sqlParam.Add(new SqlParameter("@CompanyCode", MainVariable.CompanyCode));
                sqlParam.Add(new SqlParameter("@CId", MainVariable.UserID));
                sqlParam.Add(new SqlParameter("@MId", MainVariable.UserID));
                GetConnectionString objGetConnectionString = new GetConnectionString();
                return objDbHelper.SelectInsertAndSelectDelete(ProcedureName, sqlParam, objGetConnectionString.GetNodeValues("RMRDPlus"));
                


            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                objDbHelper = null;
                sqlParam = null;
            }
        }
    }
}