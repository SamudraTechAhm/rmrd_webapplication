﻿using RMRD.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RMRD
{
    public partial class SoceitySetting : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        [WebMethod]
        public static void UpdateSocietySetting(BALSocietySetting SocietySeting)
        {

            try
            {
                SocietySeting.BALInsertAndUpdate("SpRMRDSoceityAutoConfig", SocietySeting);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally { SocietySeting = null; }
        }
        [WebMethod]
        public static string GetSocietySetting()
        {
            BALSocietySetting objBALSocietySetting = new BALSocietySetting();
            try
            {
                return objBALSocietySetting.BALSelectData("SpRMRDSoceityAutoConfig");
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally { objBALSocietySetting = null; }
        }
    }
}