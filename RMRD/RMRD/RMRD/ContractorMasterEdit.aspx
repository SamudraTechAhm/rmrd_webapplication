﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.Master" AutoEventWireup="true" CodeBehind="ContractorMasterEdit.aspx.cs" Inherits="RMRD.ContractorMasterEdit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script src="External-JS/Paging.js"></script>
    <link href="assets/css/Common-Style.css" rel="stylesheet" />
    <script src="External-JS/ContractorMaster.js"></script>
    <script src="External-JS/Object.js"></script>
    <script src="assets/js/ohsnap.min.js"></script>
    <script src="assets/js/Validation/Validation.js"></script>
    <style>
        input[type=text], textarea, select {
            -webkit-transition: all 0.30s ease-in-out;
            -moz-transition: all 0.30s ease-in-out;
            -o-transition: all 0.30s ease-in-out;
            outline: none;
            padding: 3px 0px 3px 3px;
            margin: 5px 1px 3px 0px;
        }

            input[type=text]:focus, textarea:focus, select:focus {
                box-shadow: 0 0 5px rgba(81, 203, 238, 1);
                padding: 3px 0px 3px 3px;
                margin: 5px 1px 3px 0px;
                border: 2px solid skyblue;
            }

        .alert1 {
            padding: 5px;
            margin-bottom: 5px;
            border: 1px solid #eed3d7;
            border-radius: 4px;
            position: absolute;
            bottom: 0px;
            right: 21px;
            /* Each alert has its own width */
            float: right;
            clear: right;
            color: white;
            background-color: rgba(81, 203, 238, 1);
        }

        .spinner {
            display: none;
            position: fixed;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background: rgba( 255, 255, 255, .4 ) url('Images/spinner.gif') 50% 50% no-repeat;
            overflow: hidden;
        }
    </style>
     <script type="text/javascript">
         $(document).ready(function () {
             $divspinner = $("#divspinner");
             $(document).ajaxStart(function () {
                 $divspinner.show();
             });
             $(document).ajaxStop(function () {
                 setTimeout($divspinner.hide(), 50000);
             });
             GETState();
             var ID = GetParameterValues('id');
             if (ID != null) {
                 LoadDataToForm(ID);
                 $('#btnSave').text('UPDATE')
                 $('#btnSave').val('UPDATE')
             }
             else {
                 //$("#txtRouteID").attr("readonly", false)
                 //$.each(jsonnewID, function (key, value) {
                 //    $("#txtRouteID").val(value.RtCode)
                 //});
             }
             $('input,select,textarea').on('keypress', function (e) {

                 if (e.which == 13) {
                     e.preventDefault();
                     var $next = $('[tabIndex=' + (+this.tabIndex + 1) + ']');
                     console.log($next.length);
                     if (!$next.length) {
                         $next = $('[tabIndex=1]');
                     }
                     var i = 2;
                     while ($($next).is(':disabled')) {
                         $next = $('[tabIndex=' + (+this.tabIndex + i) + ']');
                         i = i + 1;
                     }
                     $next.focus();
                 }
             });

         });


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 <div class="breadcrumbs center" id="breadcrumbs">
        <ul class="breadcrumb ">
            <li>
                <%--<i class="ace-icon fa  fa-asterisk"></i>--%>
                <span style="font-family: Verdana; font-size: 20px; color: deepskyblue">CONTRACTOR DETAIL</span>
            </li>

        </ul>

    </div>
    <div class="row">
        <div class="col-lg-2 col-sm-2"></div>
        <div class="col-lg-8 col-sm-8">
            <div class="widget-box widget-color-blue ui-sortable-handle">
                <div class="widget-header">
                   <i class="menu-icon fa fa-list-alt"></i>&nbsp;&nbsp;
                    <h5 class="widget-title">Contractor Detail</h5>

                    <div class="widget-toolbar">
                        <a href="#" data-action="collapse">
                            <i class="1 ace-icon fa fa-chevron-up bigger-125"></i>
                        </a>
                    </div>


                </div>
                <div class="widget-body">
                    <div class="widget-main alert alert-info">
                        <div class="row">
                            <div class="col-lg-2 col-sm-2">
                                <label id="lblEmployeName" class="pull-left">
                                    <h5>Contractor ID</h5>
                                </label>
                            </div>
                            <div class="col-lg-2 col-sm-2">
                                <input type="text" id="txtSupervisorID" class="form-control margin-top-4" tabindex="0" readonly="readonly"/>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-2 col-sm-2">
                                <label id="Label4" class="pull-left">
                                    <h5>Contractor Name</h5>
                                </label>
                            </div>
                            <div class="col-lg-7 col-sm-7">
                                <input type="text" id="txtName" class="required form-control margin-top-4" data-toggle="SpnSupName" tabindex="1" />
                            </div>
                            <%--<div class="col-lg-2 col-sm-2">
                                <label id="Label5" class="pull-left">
                                    <h5>Designation</h5>
                                </label>
                            </div>
                            <div class="col-lg-4 col-sm-4">
                                <input type="text" id="txtDesignation" class="required form-control margin-top-4" data-toggle="SpnDesignation" tabindex="2" />
                            </div>--%>
                        </div>
                         <div class="row">
                             <div class="col-lg-2 col-sm-2">
                                 </div>
                            <div class="col-lg-4 col-sm-4">
                                <span class="red margin-bottom-4 input pull-right" id="SpnSupName" style="display: none;"><i class="ace-icon red bigger-100 fa fa-times-circle"></i>&nbsp;Supervisior Name Is Required</span>
                            </div>
                             <div class="col-lg-6 col-sm-6">
                                <span class="red margin-bottom-4 input pull-right" id="SpnDesignation" style="display: none;"><i class="ace-icon red bigger-100 fa fa-times-circle"></i>&nbsp;Designation Is Required</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 col-sm-2">
                                <label id="Label2" class="pull-left">
                                    <h5>Contect No</h5>
                                </label>
                            </div>
                            <div class="col-lg-4 col-sm-4">
                                <input type="text" id="txtContectNo" class="Numaric required form-control margin-top-4" tabindex="3" data-toggle="SpnPhno" maxlength="10"/>
                            </div>
                            <div class="col-lg-2 col-sm-2">
                                <label id="Label3" class="pull-left">
                                    <h5>Email ID</h5>
                                </label>
                            </div>
                            <div class="col-lg-4 col-sm-4">
                                <input type="text" id="txtEmail" class="Email form-control margin-top-4" tabindex="4" data-toggle="spnEmail" />
                            </div>
                        </div>
                        
                        <div class="row">
                             <div class="col-lg-2 col-sm-2">
                                 </div>
                            <div class="col-lg-4 col-sm-4">
                                <span class="red margin-bottom-4 input pull-right" id="SpnPhno" style="display: none;"><i class="ace-icon red bigger-100 fa fa-times-circle"></i>&nbsp;Contact No Is Required</span>
                            </div>
                            <div class="col=lg-6 col-sm-6">
                                <span class="red margin-bottom-4 input pull-right" id="spnEmail" style="display: none;"><i class="ace-icon red bigger-140 fa fa-times-circle"></i></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 col-sm-2">
                                <label id="Label1" class="pull-left">
                                    <h5>Gender</h5>
                                </label>
                            </div>
                            <div class="col-lg-4 col-sm-4">
                                <div class="control-group">
                                    <div class="radio">
                                        <label>
                                            <input class="ace" name="form-field-radio" type="radio" id="RadioMale" tabindex="5">
                                            <span class="lbl">Male</span>
                                        </label>
                                        <label>
                                            <input class="ace" name="form-field-radio" type="radio" id="RadioFeMale" tabindex="6">
                                            <span class="lbl">Female</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-sm-2">
                                <label id="Label6" class="pull-left">
                                    <h5>BirthDate</h5>
                                </label>
                            </div>
                            <div class="col-lg-4 col-sm-4">

                                <input id="txtBDate" class="required form-control date-picker" data-date-format="dd/mm/yyyy" type="text" data-toggle="SpnBDate" tabindex="7">

                            </div>
                        </div>
                         <div class="row">
                             <div class="col-lg-6 col-sm-6">
                                 </div>                            
                             <div class="col-lg-6 col-sm-6">
                                <span class="red margin-bottom-4 input pull-right" id="SpnBDate" style="display: none;"><i class="ace-icon red bigger-100 fa fa-times-circle"></i>&nbsp;Birthdate Is Required</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 col-sm-2">
                                <label id="Label8" class="pull-left">
                                    <h5>Address 1</h5>
                                </label>
                            </div>
                            <div class="col-lg-7 col-sm-7">
                                <input type="text" id="txtAdd1" class="required form-control margin-top-4" tabindex="8" placeholder="Address 1" data-toggle="SpnAdd1" />
                            </div>

                            <div class="col-lg-3 col-sm-3">
                                <input type="text" id="txtPin" class="Numaric form-control margin-top-4" tabindex="9" placeholder="PIN Code" />
                            </div>
                        </div>
                        <div class="row">
                             <div class="col-lg-2 col-sm-2">
                                 </div>                            
                             <div class="col-lg-7 col-sm-7">
                                <span class="red margin-bottom-4 input pull-right" id="SpnAdd1" style="display: none;"><i class="ace-icon red bigger-100 fa fa-times-circle"></i>&nbsp;Address Required</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 col-sm-2">
                                <label id="Label7" class="pull-left">
                                    <h5>Address 2</h5>
                                </label>
                            </div>
                            <div class="col-lg-4 col-sm-4">
                                <input type="text" id="txtCity" class="required form-control margin-top-4" tabindex="10" placeholder="City" data-toggle="SpnCity" />
                            </div>
                            <div class="col-lg-3 col-sm-3">
                                <select id="ddlState" class="form-control required" data-toggle="SpnState" tabindex="11">
                                    <option value="0">SELECT STATE</option>
                                </select>
                            </div>
                            <div class="col-lg-3 col-sm-3">
                                <input type="text" id="txtCountry" class="form-control margin-top-4" tabindex="12" placeholder="Country" />
                            </div>
                        </div>
                        <div class="row">
                             <div class="col-lg-2 col-sm-2">
                                 </div>                            
                             <div class="col-lg-4 col-sm-4">
                                <span class="red margin-bottom-4 input pull-right" id="SpnCity" style="display: none;"><i class="ace-icon red bigger-100 fa fa-times-circle"></i>&nbsp;City Name Required</span>
                            </div>
                            <div class="col-lg-3 col-sm-3">
                                <span class="red margin-bottom-4 input pull-right" id="SpnState" style="display: none;"><i class="ace-icon red bigger-100 fa fa-times-circle"></i>&nbsp;State Name Required</span>
                            </div>
                        </div>
                    </div>

                    <div class="widget-toolbox padding-8 clearfix">
                        <div class="row">
                            <div class="col-lg-4 col-sm-4">

                                <button class="btn btn-xs btn-danger pull-left" type="button" id="btnBack" onclick="BackClick()" >
                                    <i class="ace-icon fa fa-arrow-left icon-on-left"></i>
                                    <span class="bigger-110">Back</span>
                                </button>
                            </div>
                            <div class="col-lg-4 col-sm-4">
                                <br />
                                <br />
                                <div id="ohsnap"></div>
                            </div>
                            <div class="col-lg-4 col-sm-4">

                                <button class="btn btn-xs btn-info  pull-right" type="button" onclick="btnSave_click()" value="Save" tabindex="13" id="btnSave">
                                    <i class="ace-icon fa fa-floppy-o white"></i>
                                    <span class="bigger-110">SAVE</span>

                                </button>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        <div class="col-lg-2 col-sm-2"></div>
        <%--<div class="row"><div id="ohsnap"></div></div>--%>
    </div>


    <div class="spinner" id="divspinner"></div>

</asp:Content>
