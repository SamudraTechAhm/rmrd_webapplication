﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.Master" AutoEventWireup="true" CodeBehind="RouteMaster.aspx.cs" Inherits="RMRD.RouteMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <script src="External-JS/Paging.js"></script>
    <link href="assets/css/Common-Style.css" rel="stylesheet" />
    <script src="External-JS/RouteMaster.js"></script>
    <script src="assets/js/ohsnap.min.js"></script>
    <style>
        input[type=text], textarea, select {
            -webkit-transition: all 0.30s ease-in-out;
            -moz-transition: all 0.30s ease-in-out;
            -o-transition: all 0.30s ease-in-out;
            outline: none;
            padding: 3px 0px 3px 3px;
            margin: 5px 1px 3px 0px;
            border: 2px solid #DDDDDD;
        }

            input[type=text]:focus, textarea:focus, select:focus {
                box-shadow: 0 0 5px rgba(81, 203, 238, 1);
                padding: 3px 0px 3px 3px;
                margin: 5px 1px 3px 0px;
                border: 2px solid rgba(81, 203, 238, 1);
            }

        .alert1 {
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid #eed3d7;
            border-radius: 4px;
            position: absolute;
            bottom: 0px;
            right: 21px;
            /* Each alert has its own width */
            float: right;
            clear: right;
            color: white;
            background-color: #DA4453;
        }

        .spinner {
            display: none;
            position: fixed;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background: rgba( 255, 255, 255, .4 ) url('Images/spinner.gif') 50% 50% no-repeat;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript">
        var EFFCODE;
        $(document).ready(function () {
            $("#drpRouteDate").focus();
            GETRouteDate();

            $("#btnOk").click(function () {
                $divspinner = $("#divspinner");
                $(document).ajaxStart(function () {
                    $divspinner.show();
                });
                $(document).ajaxStop(function () {
                    setTimeout($divspinner.hide(), 50000);
                });
                if ($("#drpRouteDate").val().trim() == "0") {
                    ohSnap('Please Select Valid Valid Effective Date !!');
                }
                else {
                    EFFCODE = $("#txtCode").val().trim()
                    $("#RouteDetail").removeClass("hide");
                    FillGrid();
                }
                //$(window).scrollTop($("#txtAmountRecoverable").offset().top);
            });


            $("#drpRouteDate").on("change", function () {
                var MemCode = $("#drpRouteDate option:selected").attr("value");
                $("#txtCode").val(MemCode);

            });
            $('input,select,textarea').on('keypress', function (e) {

                if (e.which == 13) {
                    e.preventDefault();
                    var $next = $('[tabIndex=' + (+this.tabIndex + 1) + ']');
                    console.log($next.length);
                    if (!$next.length) {
                        $next = $('[tabIndex=1]');
                    }
                    var i = 2;
                    while ($($next).is(':disabled')) {
                        $next = $('[tabIndex=' + (+this.tabIndex + i) + ']');
                        i = i + 1;
                    }
                    $next.focus();
                }
            });
        });

        function FillGrid() {
            var EffCode = null;

            EffCode = $('#txtCode').val();

            $.ajax({
                type: "POST",
                url: "RouteMaster.aspx/GetRoute",
                data: "{ EffCode: '" + EffCode + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {

                    var json = JSON.parse(data.d);
                    $.each(json, function (key, value) {
                        //var col1 = '<tr><td onclick="Edit_Row($(this))" class="td width-100 btn btn-block btn-flat center"><i class="ace-icon fa fa-edit blue bigger-150"></i></td>';
                        var col1 = '<tr><td onclick="Edit_Row($(this))" class="td width-100 btn btn-block btn-flat center"><i class="ace-icon fa fa-pencil-square-o bigger-150"></i></td>';
                        var col2 = '<td class="td code center">' + value.RouteCode + '</td>';
                        var col3 = '<td class="td  left">' + value.RouteName + '</td>';
                        var col4 = '<td class="td  center">' + value.SchTimeMor + '</td>';
                        var col5 = '<td class="td  center">' + value.SchTimeEve + '</td>';
                        var col6 = '<td class="td  center ">' + value.CMM + '</td>';
                        $("tbody").append(col1 + col2 + col3 + col4 + col5 + col6);
                    });
                    Paging('TblRouteDetail');
                },
                failure: function (response) {
                    alert("failure");
                },
                error: function (response) {
                    alert("error");
                }
            });
        }

        function Edit_Row(row) {
            var $item = row.closest("tr").find(".code").text();
            var url = "RouteMasterEdit.aspx?id=" + $item + "&EFFCODE=" + EFFCODE;
            window.location = url;
        }
        function Add_click() {
            //alert(EFFCODE)
            window.location = "RouteMasterEdit.aspx?EFFCODE=" + EFFCODE;
        }

    </script>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-lg-12">
            <div class="widget-header">
                <div class="row margin-top-4">
                    <%--<div class="col-lg-12">--%>
                    <div class="col-lg-2"></div>
                    <div class="col-lg-8">
                        <div class="col-lg-3">
                            <label class="pull-left" id="Label24">
                                <h4>Effective Date:</h4>
                            </label>
                        </div>
                        <div class="col-lg-3">
                            <select class="form-control" id="drpRouteDate" tabindex="0">
                                <option value="0">--SELECT--</option>

                            </select>
                        </div>
                        <div class="col-lg-3">
                            <input type="text" disabled="disabled" class="form-control margin-top-4" id="txtCode" />
                        </div>
                        <div class="col-lg-3">
                            <button id="btnOk" type="button" style="width: 150px" class="btn btn-success pull-right" tabindex="1">OK</button>
                        </div>
                    </div>
                    <div class="col-lg-2"></div>

                </div>
                <%--</div>--%>
            </div>
            <br />
            <div class="row hide" id="RouteDetail">
                <div class="col-lg-2"></div>
                <div class="col-lg-8 col-sm-8" style="margin-left: 20px">
                    <div class="widget-box">
                        <div class="widget-header">
                            <h4 class="widget-title">Route Master Detail</h4>

                            <span class="widget-toolbar">

                                <a data-action="reload" href="#">
                                    <i class="ace-icon fa fa-refresh"></i>
                                </a>

                                <a data-action="collapse" href="#">
                                    <i class="ace-icon fa fa-chevron-up"></i>
                                </a>
                            </span>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main">
                                <div class="row">
                                    <div class="col-lg-8"></div>

                                     <div class="col-xs-2">
                                        <div>
                                            <input placeholder="Search..." type="text" id="Text1" class="input form-control Search " data-toggle="TblRouteDetail" />
                                        </div>
                                    </div>
                                    <div class="col-xs-2">
                                        <button class="btn-white btn col-xs-12 " type="button" onclick="Add_click()">
                                            <i class="fa fa-plus"></i>&nbsp;Add
                                        </button>
                                    </div>
                                </div>
                                </br>
                                 <div class="box-body">
                                     <div class="box-body scroll--200">
                                         <table id="TblRouteDetail" class="table">
                                             <thead class="bg-blue-light">
                                                 <tr>
                                                     <th class="td center"></th>
                                                     <th class="td center">Route Code</th>
                                                     <th class="td center">Route Name</th>
                                                     <th class="td center">Morning Time</th>
                                                     <th class="td center">Evening Time</th>
                                                     <th class="td center">Center ID</th>
                                                 </tr>
                                             </thead>
                                             <tbody class=""></tbody>
                                         </table>
                                     </div>
                                     <div class="center">
                                         <div class="space-4 tbl-footer-bg"></div>
                                         <div class="tbl-footer-bg">
                                             <button onclick="FirstPage('TblRouteDetail')" class="margin-bottom-4 margin-top-4 btn btn-sm btn-tumblr" type="button"><i class="ace-icon fa fa-angle-double-left"></i></button>
                                             <button onclick="PreviousPage('TblRouteDetail')" class="margin-bottom-4 margin-top-4 btn btn-sm btn-tumblr" type="button"><i class="ace-icon fa fa-angle-left"></i></button>
                                             <b class="bigger-100">|</b>&nbsp;&nbsp;<span dir="ltr">Page<span id="sp_0_grid-pager">1</span>of <span id="sp_1_grid-pager">23</span></span>
                                             <b class="bigger-100">|</b>&nbsp;&nbsp;
                                             <button onclick="NextPage('TblRouteDetail')" class="margin-bottom-4 margin-top-4 btn btn-sm btn-tumblr" type="button"><i class="ace-icon fa fa-angle-right"></i></button>
                                             <button onclick="LastPage('TblRouteDetail')" class="margin-bottom-4 margin-top-4 btn btn-sm btn-tumblr" type="button"><i class="ace-icon fa fa-angle-double-right"></i></button>
                                         </div>
                                         <div class="space-4 tbl-footer-bg"></div>
                                     </div>

                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2"></div>
            </div>
        </div>

        <div class="spinner" id="divspinner"></div>
    </div>
    <div class="row">

        <div id="ohsnap"></div>
    </div>
</asp:Content>
