﻿using System;
using RMRD.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace RMRD
{
    public partial class MobileMasterEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static string GetMobileInformation()
        {
            BALMobileMaster objBALMobileMaster = null;
            try
            {
                objBALMobileMaster = new BALMobileMaster();
                return objBALMobileMaster.BALSelectData("spRMRDMobileMasterEdit");
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally { objBALMobileMaster = null; }
        }
    }
}