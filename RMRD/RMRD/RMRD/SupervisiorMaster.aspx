﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.Master" AutoEventWireup="true" CodeBehind="SupervisiorMaster.aspx.cs" Inherits="RMRD.SupervisiorMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="External-JS/Paging.js"></script>
    <link href="assets/css/Common-Style.css" rel="stylesheet" />
    <script src="External-JS/SupervisiorMaster.js"></script>
    <script src="assets/js/ohsnap.min.js"></script>
    <style>
        input[type=text], textarea, select {
            -webkit-transition: all 0.30s ease-in-out;
            -moz-transition: all 0.30s ease-in-out;
            -o-transition: all 0.30s ease-in-out;
            outline: none;
            padding: 3px 0px 3px 3px;
            margin: 5px 1px 3px 0px;
            border: 2px solid #DDDDDD;
        }

            input[type=text]:focus, textarea:focus, select:focus {
                box-shadow: 0 0 5px rgba(81, 203, 238, 1);
                padding: 3px 0px 3px 3px;
                margin: 5px 1px 3px 0px;
                border: 2px solid rgba(81, 203, 238, 1);
            }

        .alert1 {
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid #eed3d7;
            border-radius: 4px;
            position: absolute;
            bottom: 0px;
            right: 21px;
            /* Each alert has its own width */
            float: right;
            clear: right;
            color: white;
            background-color: #DA4453;
        }

        .spinner {
            display: none;
            position: fixed;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background: rgba( 255, 255, 255, .4 ) url('Images/spinner.gif') 50% 50% no-repeat;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $divspinner = $("#divspinner");
            $(document).ajaxStart(function () {
                $divspinner.show();
            });
            $(document).ajaxStop(function () {
                setTimeout($divspinner.hide(), 50000);
            });
            FillGrid();
            //FillGrid();
            $("#btnBack").click(function () {
                window.location = "WebForm1.aspx";
            })
        });


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="breadcrumbs center" id="breadcrumbs">

        <ul class="breadcrumb ">
            <li>
                <i class="ace-icon fa  fa-asterisk"></i>
                <a href="#" style="font-family: Calibri; font-size: larger">SUPERVISIOR</a>
            </li>

        </ul>
    </div>
    <div class="widget-box widget-color-blue ui-sortable-handle">
        <div class="widget-header">
            <h5 class="widget-title">Supervisior Detail's</h5>

            <div class="widget-toolbar">
                <a href="#" data-action="collapse">
                    <i class="1 ace-icon fa fa-chevron-up bigger-125"></i>
                </a>
            </div>

            <div class="widget-toolbar no-border">
               
                <div id="nav-search" class="nav-search">
                    <form class="form-search">

                        <span class="input-icon">
                            <%--<i class="ace-icon fa fa-search nav-search-icon"></i>--%>
                            <input placeholder="Search..." type="text" id="nav-search-input" class="nav-search-input Search" data-toggle="TblSupervisiorDetail" autocomplete="off" />
                            <%--<input id="nav-search-input" class="nav-search-input" placeholder="Search ..." autocomplete="off" type="text">--%>
                            
                        </span>
                    </form>
                </div>

            </div>
        </div>
        <div class="widget-body">
            <div class="widget-main ">
                <div class="box-body scroll--200 ">
                    <table id="TblSupervisiorDetail" class="table">
                        <thead class="bg-blue-light">
                            <tr>
                                <th class="td center"></th>
                                <th class="td center">Code</th>
                                <th class="td center">Name</th>
                                <th class="td center">Designation</th>
                                <th class="td center">Mobile No</th>
                                <th class="td center">Birth Date</th>
                                <th class="td center">City</th>
                            </tr>
                        </thead>
                        <tbody class=""></tbody>
                    </table>
                </div>
                <div class="center">
                    <div class="space-4 tbl-footer-bg"></div>
                    <div class="tbl-footer-bg">
                        <button onclick="FirstPage('TblSupervisiorDetail')" class="margin-bottom-4 margin-top-4 btn btn-sm btn-tumblr" type="button"><i class="ace-icon fa fa-angle-double-left"></i></button>
                        <button onclick="PreviousPage('TblSupervisiorDetail')" class="margin-bottom-4 margin-top-4 btn btn-sm btn-tumblr" type="button"><i class="ace-icon fa fa-angle-left"></i></button>
                        <b class="bigger-100">|</b>&nbsp;&nbsp;<span dir="ltr">Page<span id="sp_0_grid-pager">1</span>of <span id="sp_1_grid-pager">23</span></span>
                        <b class="bigger-100">|</b>&nbsp;&nbsp;
                                             <button onclick="NextPage('TblSupervisiorDetail')" class="margin-bottom-4 margin-top-4 btn btn-sm btn-tumblr" type="button"><i class="ace-icon fa fa-angle-right"></i></button>
                        <button onclick="LastPage('TblSupervisiorDetail')" class="margin-bottom-4 margin-top-4 btn btn-sm btn-tumblr" type="button"><i class="ace-icon fa fa-angle-double-right"></i></button>
                    </div>
                    <div class="space-4 tbl-footer-bg"></div>
                </div>
                <%--<p class="alert alert-info">
                    Nunc aliquam enim ut arcu aliquet adipiscing. Fusce dignissim volutpat justo non consectetur. Nulla fringilla eleifend consectetur.
                </p>--%>
            </div>

            <div class="widget-toolbox padding-8 clearfix">
                <div class="row">
                    <div class="col-lg-6">

                        <button class="btn btn-xs btn-danger pull-left" type="button" id="btnBack" onclick="BtnBackClick()">
                            <i class="ace-icon fa fa-arrow-left icon-on-left"></i>
                            <span class="bigger-110">Back</span>
                        </button>
                    </div>
                    <div class="col-lg-6">
                        <button class="btn btn-xs btn-success pull-right" type="button" onclick="BtnNewClick()">
                            <span class="bigger-110">New Entry</span>
                            <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                        </button>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <div class="spinner" id="divspinner"></div>

</asp:Content>
