﻿using RMRD.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RMRD
{
    public partial class ContractorMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static string GetContractor()
        {
            BALContractorMaster objBALContractorMaster = null;
            try
            {
                objBALContractorMaster = new BALContractorMaster();
                return objBALContractorMaster.BALSelectAllContractor("SpRMRDContractorMaster", 0);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally { objBALContractorMaster = null; }
        }
    }
}