﻿using RMRD.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RMRD
{
    public partial class SupervisiorMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static string GetSupervisior()
        {
            BALSupervisiorMaster objBALSupervisiorMaster = null;
            try
            {
                objBALSupervisiorMaster = new BALSupervisiorMaster();
                return objBALSupervisiorMaster.BALSelectAllSuperVisiour("SpRMRDSupervisiorMaster",0);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally { objBALSupervisiorMaster = null; }
        }
        
    }
}