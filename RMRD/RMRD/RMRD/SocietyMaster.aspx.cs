﻿using RMRD.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RMRD
{
    public partial class SocietyMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
             
        }
        [WebMethod]
        public static string GetSocietyDetails()
        {
            BALSocietyMaster objBALSoceityMaster = null;
            try
            {
                objBALSoceityMaster = new BALSocietyMaster();
                return objBALSoceityMaster.BALSelectData("SpRMRDSoceityMaster");
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally { objBALSoceityMaster = null; }
        }
    }
}