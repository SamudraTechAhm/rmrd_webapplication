﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.Master" AutoEventWireup="true" CodeBehind="TenkarMasterEdit.aspx.cs" Inherits="RMRD.TenkarMasterEdit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="External-JS/Paging.js"></script>
    <link href="assets/css/Common-Style.css" rel="stylesheet" />    
    <script src="External-JS/TenkerMaster.js"></script>
    <script src="assets/js/ohsnap.min.js"></script>
    <script src="External-JS/Object.js"></script>
    <script src="assets/js/Validation/Validation.js"></script>
      <style>
        input[type=text], textarea, select {
            -webkit-transition: all 0.30s ease-in-out;
            -moz-transition: all 0.30s ease-in-out;
            -o-transition: all 0.30s ease-in-out;
            outline: none;
            padding: 3px 0px 3px 3px;
            margin: 5px 1px 3px 0px;
            border: 2px solid #dddddd;
        }

            input[type=text]:focus, textarea:focus, select:focus {
                box-shadow: 0 0 5px rgba(81, 203, 238, 1);
                padding: 3px 0px 3px 3px;
                margin: 5px 1px 3px 0px;
                border: 2px solid rgba(81, 203, 238, 1);
            }

        .alert1 {
            padding: 5px;
            margin-bottom: 5px;
            border: 1px solid #eed3d7;
            border-radius: 4px;
            position: absolute;
            bottom: 0px;
            right: 21px;
            /* Each alert has its own width */
            float: right;
            clear: right;
            color: white;
            background-color: rgba(81, 203, 238, 1);
        }

        .spinner {
            display: none;
            position: fixed;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background: rgba( 255, 255, 255, .4 ) url('Images/spinner.gif') 50% 50% no-repeat;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            //setTimeout(function () { $("#abc").click(); }, 100);
            
            $divspinner = $("#divspinner");
            $(document).ajaxStart(function () {
                $divspinner.show();
            });
            $(document).ajaxStop(function () {
                setTimeout($divspinner.hide(), 50000);

            });
            GETContractor();
            var ID = GetParameterValues('id');
            if (ID != null) {
                LoadDataToForm(ID);
                $('#btnSave').text('UPDATE')
                $('#btnSave').val('UPDATE')
            }
            else {
                //$("#txtRouteID").attr("readonly", false)
                //$.each(jsonnewID, function (key, value) {
                //    $("#txtRouteID").val(value.RtCode)
                //});
            }

            $('input,select,textarea').on('keypress', function (e) {

                if (e.which == 13) {
                    e.preventDefault();
                    var $next = $('[tabIndex=' + (+this.tabIndex + 1) + ']');
                    console.log($next.length);
                    if (!$next.length) {
                        $next = $('[tabIndex=1]');
                    }
                    var i = 2;
                    while ($($next).is(':disabled')) {
                        $next = $('[tabIndex=' + (+this.tabIndex + i) + ']');
                        i = i + 1;
                    }
                    $next.focus();
                }
            });
        });

        function BtnNewClick() {
            
        }
        function BackClick() {
            window.location.href = "TenkerMaster.aspx"
        }
        function BtnBackClick() {
            window.location.href = "WebForm1.aspx"
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="breadcrumbs center" id="breadcrumbs">

        <ul class="breadcrumb ">
            <li>
                <i class="ace-icon fa  fa-asterisk"></i>
                <a href="#" style="font-family: Calibri; font-size: larger">TENKER / VEHICAL DETAIL</a>
            </li>

        </ul>
    </div>
      <div class="row">
          <div class="col-sm-2 col-sm-2" ></div>
         <div class="col-lg-8 col-sm-8">
                    <div class="widget-box widget-color-blue ui-sortable-handle">
                        <div class="widget-header">
                            <i class="menu-icon fa fa-list-alt"></i>&nbsp;&nbsp;
                    <h5 class="widget-title">Vehical Detail</h5>

                            <div class="widget-toolbar">
                                <a href="#" id="a1" data-action="collapse">
                                    <i class="1 ace-icon fa fa-chevron-up bigger-125"></i>
                                </a>
                            </div>


                        </div>
                        <div class="widget-body">
                            <div class="widget-main alert alert-info">
                                <div class="row">
                                    <div class="col-lg-3 col-sm-3">
                                        <label id="lblEmployeName" class="pull-left">
                                            <h5>Vehical ID</h5>
                                        </label>
                                    </div>
                                    <div class="col-lg-2 col-sm-2">
                                        <input type="text" id="txtVehicalID" class="form-control margin-top-4" tabindex="0" readonly="readonly" />
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-lg-3 col-sm-3">
                                        <label id="Label4" class="pull-left">
                                            <h5>Vehical Name</h5>
                                        </label>
                                    </div>
                                    <div class="col-lg-7 col-sm-7">
                                        <input type="text" id="txtVName" class="required form-control margin-top-4" data-toggle="SpnVName" tabindex="1" />
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-lg-3 col-sm-3">
                                    </div>

                                    <div class="col-lg-7 col-sm-7">
                                        <span class="red margin-bottom-4 input pull-right" id="SpnVName" style="display: none;"><i class="ace-icon red bigger-100 fa fa-times-circle"></i>&nbsp;Vehical Name Is Required</span>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3 col-sm-3">
                                        <label id="Label1" class="pull-left">
                                            <h5>Vihical Type</h5>
                                        </label>
                                    </div>
                                    <div class="col-lg-7 col-sm-7">
                                        <input type="text" id="txtType" class="required form-control margin-top-4" data-toggle="SpnType" tabindex="1" />
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-lg-3 col-sm-3">
                                    </div>

                                    <div class="col-lg-7 col-sm-7">
                                        <span class="red margin-bottom-4 input pull-right" id="SpnType" style="display: none;"><i class="ace-icon red bigger-100 fa fa-times-circle"></i>&nbsp;Vehical Type Is Required</span>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3 col-sm-3">
                                        <label id="Label2" class="pull-left">
                                            <h5>Vihical Capacity</h5>
                                        </label>
                                    </div>
                                    <div class="col-lg-7 col-sm-7">
                                        <input type="text" id="txtCapacity" class="required form-control margin-top-4" data-toggle="SpnCapacity" tabindex="1" />
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-lg-3 col-sm-3">
                                    </div>

                                    <div class="col-lg-7 col-sm-7">
                                        <span class="red margin-bottom-4 input pull-right" id="SpnCapacity" style="display: none;"><i class="ace-icon red bigger-100 fa fa-times-circle"></i>&nbsp;Vehical Capacity Is Required</span>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3 col-sm-3">
                                        <label id="Label5" class="pull-left">
                                            <h5>Contractor Name</h5>
                                        </label>
                                    </div>
                                    <div class="col-lg-7 col-sm-7">
                                        <select id="ddlContractor" class="form-control required" data-toggle="SpnContractor" tabindex="2">
                                            <option value="0">SELECT CONTRACTOR</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-lg-3 col-sm-3">
                                    </div>

                                    <div class="col-lg-7 col-sm-7">
                                        <span class="red margin-bottom-4 input pull-right" id="SpnContractor" style="display: none;"><i class="ace-icon red bigger-100 fa fa-times-circle"></i>&nbsp;Contractor Name Is Required</span>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3 col-sm-3">
                                        <label id="Label3" class="pull-left">
                                            <h5>Contect No</h5>
                                        </label>
                                    </div>
                                    <div class="col-lg-7 col-sm-7">
                                        <input type="text" id="txtMobile" maxlength="10" class="Numaric form-control margin-top-4" tabindex="1" />
                                    </div>

                                </div>
                               
                                
                              
                            </div>

                            <div class="widget-toolbox padding-8 clearfix">
                                <div class="row">
                                    <div class="col-lg-4 col-sm-4">

                                        <button class="btn btn-xs btn-danger pull-left" type="button" id="btnBack" onclick="BackClick()">
                                            <i class="ace-icon fa fa-arrow-left icon-on-left"></i>
                                            <span class="bigger-110">Back</span>
                                        </button>
                                    </div>
                                    <div class="col-lg-4 col-sm-4">
                                        <br />
                                        <br />
                                        <div id="ohsnap"></div>
                                    </div>
                                    <div class="col-lg-4 col-sm-4">

                                        <button class="btn btn-xs btn-info  pull-right" type="button" onclick="btnSave_click()" value="Save" tabindex="3" id="btnSave">
                                            <i class="ace-icon fa fa-floppy-o white"></i>
                                            <span class="bigger-110">SAVE</span>

                                        </button>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
          <div class="col-sm-2 col-sm-2" ></div>
    </div>

    <div class="spinner" id="divspinner"></div>
</asp:Content>
