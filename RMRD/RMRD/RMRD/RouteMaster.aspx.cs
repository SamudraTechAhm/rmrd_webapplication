﻿using RMRD.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RMRD
{
    public partial class RouteMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static string GetRoute(Int32 EffCode)
        {
            BALRouteMaster objBALRouteMaster = null;
            try
            {
                objBALRouteMaster = new BALRouteMaster();
                return objBALRouteMaster.BALSelectRouteMaster("Sp_RoughtMaster", EffCode);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally { objBALRouteMaster = null; }
        }
    }
}