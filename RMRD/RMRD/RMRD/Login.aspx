﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="RMRD.Login" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Login Form</title>
    <link href="css/style.css" rel="stylesheet" />
    <style type="text/css">
        .centered
        {
            position: fixed;
            top: 50%;
            left: 50%;
            /* bring your own prefixes */
            transform: translate(-50%, -50%);
        }

        #footer
        {
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 30px; /* Height of the footer */
            background: #6cf;
        }
    </style>
    <script>
        function login_Click()
        {
            window.location.href = "WebForm1.aspx"
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <section class="">
            <div class="login centered">
                <h1>Milk Bill System</h1>
                <p>
                    <asp:TextBox ID="TxtUserName" runat="server" placeholder="Username"></asp:TextBox>
                </p>
                <p>
                    <asp:TextBox ID="TxtPassword" runat="server" placeholder="Password" TextMode="Password"></asp:TextBox>
                </p>
                <p class="remember_me">
                    <label>
                        <%-- <input type="checkbox" name="remember_me" id="remember_me">--%><img class="login-box" src="../assets/images/logo11.jpg" id="imagelogo">
                    </label>
                </p>
                <p class="submit">
                    <asp:Button runat="server" ID="login" Text="Login" OnClientClick="login_Click()" />

                </p>

            </div>
            <div id="footer">
                <font color="black"><b>Milk Bill System © 2015-2016</b></font>
            </div>
        </section>
    </form>
</body>
</html>