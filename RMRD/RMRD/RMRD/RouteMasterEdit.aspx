﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.Master" AutoEventWireup="true" CodeBehind="RouteMasterEdit.aspx.cs" Inherits="RMRD.RouteMasterEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="External-JS/Object.js"></script>
    <script src="External-JS/RouteMaster.js"></script>
    <script src="assets/js/Validation/Validation.js"></script>
    <script src="assets/js/ohsnap.min.js"></script>
    <style>
        input[type=text], textarea,select {
            -webkit-transition: all 0.30s ease-in-out;
            -moz-transition: all 0.30s ease-in-out;
            -o-transition: all 0.30s ease-in-out;
            outline: none;
            padding: 3px 0px 3px 3px;
            margin: 5px 1px 3px 0px;
            border: 2px solid #DDDDDD;
        }

            input[type=text]:focus, textarea:focus ,select:focus{
                box-shadow: 0 0 5px rgba(81, 203, 238, 1);
                padding: 3px 0px 3px 3px;
                margin: 5px 1px 3px 0px;
                border: 2px solid rgba(81, 203, 238, 1);
            }

        .alert1 {
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid #eed3d7;
            border-radius: 4px;
            position: absolute;
            bottom: 0px;
            right: 21px;
            /* Each alert has its own width */
            float: right;
            clear: right;
            color: white;
            background-color: #37BC9B;
        }

       
        .spinner {
            display: none;
            position: fixed;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background: rgba( 255, 255, 255, .4 ) url('Images/spinner.gif') 50% 50% no-repeat;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#txtRouteName").focus();
            $divspinner = $("#divspinner");
            $(document).ajaxStart(function () {
                $divspinner.show();
            });
            $(document).ajaxStop(function () {
                setTimeout($divspinner.hide(), 50000);
            });
            SelectData();
            var ID = GetParameterValues('id');
            if (ID != null) {
                LoadDataToForm(ID);
                $('#btnSave').text('UPDATE')
                $('#btnSave').val('UPDATE')
            }
            else {
                $.each(jsonnewID, function (key, value) {
                    $("#txtRouteID").val(value.RtCode)
                });
            }
            $('input,select,textarea').on('keypress', function (e) {

                if (e.which == 13) {
                    e.preventDefault();
                    var $next = $('[tabIndex=' + (+this.tabIndex + 1) + ']');
                    console.log($next.length);
                    if (!$next.length) {
                        $next = $('[tabIndex=1]');
                    }
                    var i = 2;
                    while ($($next).is(':disabled')) {
                        $next = $('[tabIndex=' + (+this.tabIndex + i) + ']');
                        i = i + 1;
                    }
                    $next.focus();
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div class="row">
        <div class="col-lg-3">
            
        </div>
        <div class="col-lg-6">
            <div class="space-10"></div>
            <div class="space-10"></div>
            <div class="space-10"></div>
            
            
            <div class="row">     
                <div class="col-xs-11 col-sm-11" style="margin-left: 20px">
                    <div class="widget-box">
                        <div class="widget-header">
                            <h4 class="widget-title">Route Master Detail</h4>
                            <span class="widget-toolbar">
                                <a data-action="reload" href="#">
                                    <i class="ace-icon fa fa-refresh"></i>
                                </a>
                                <a data-action="collapse" href="#">
                                    <i class="ace-icon fa fa-chevron-up"></i>
                                </a>
                            </span>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label id="lblEmployeName" class="pull-left">
                                            <h5>Route ID</h5>
                                        </label>
                                    </div>
                                    <div class="col-lg-3">
                                        <input type="text" id="txtRouteID" class="form-control margin-top-4" tabindex="0" readonly="readonly"/>
                                    </div>
                                    <div class="col-lg-3">
                                        <label id="Label1" class="pull-left">
                                            <h5>Route Name</h5>
                                        </label>
                                    </div>
                                    <div class="col-lg-3">
                                        <input type="text" id="txtRouteName" class="required form-control margin-top-4" data-toggle="spnRouteName" tabindex="1"/>
                                    </div>

                                </div>
                                <%--<div class="space-8"></div>--%>

                                <div class="row">
                                    <div class="col-lg-6">
                                    </div>
                                    <div class="col-lg-6">
                                        <span class="red margin-bottom-4 input pull-right" id="spnRouteName" style="display: none;"><i class="ace-icon red bigger-140 fa fa-times-circle"></i>&nbsp;Route Name Is Required</span>
                                    </div>
                                </div>
                                <div class="space-6"></div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label id="Label2" class="pull-left">
                                            <h5>Center Name</h5>
                                        </label>
                                    </div>
                                    <div class="col-lg-3">
                                        <select id="ddlCenter" class="form-control required" data-toggle="SpnCenter" tabindex="2">
                                            <option value="0">SELECT</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                        <label id="Label3" class="pull-left">
                                            <h5>Vehical No</h5>
                                        </label>
                                    </div>
                                    <div class="col-lg-3">
                                        <select id="ddlTruckNo" class="form-control required" data-toggle="SpnVehical" tabindex="3">
                                            <option value="0">SELECT</option>
                                        </select>
                                    </div>
                                </div>
                                <%--<div class="space-6"></div>--%>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <span class="red margin-bottom-4 input pull-right" id="SpnCenter" style="display: none;"><i class="ace-icon red bigger-140 fa fa-times-circle"></i>&nbsp;Center Name Is Required</span>
                                    </div>
                                    <div class="col-lg-6">
                                        <span class="red margin-bottom-4 input pull-right" id="SpnVehical" style="display: none;"><i class="ace-icon red bigger-140 fa fa-times-circle"></i>&nbsp;Vehical No Is Required</span>
                                    </div>
                                </div>
                                <div class="space-6"></div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label id="Label4" class="pull-left">
                                            <h5>Field Officer</h5>
                                        </label>
                                    </div>
                                    
                                    <div class="col-lg-9">
                                        <select id="ddlFieldOff" class="form-control required" data-toggle="SpnOfficer" tabindex="4">
                                            <option value="0">SELECT</option>
                                        </select>
                                    </div>
                                  
                                </div>
                                <%--<div class="space-6"></div>--%>
                                <div class="row">
                                    <div class="col-lg-6">
                                        
                                    </div>
                                   
                                    <div class="col-lg-6">
                                        <span class="red margin-bottom-4 input pull-right" id="SpnOfficer" style="display: none;"><i class="ace-icon red bigger-140 fa fa-times-circle"></i>&nbsp;Officer Name Is Required</span>
                                    </div>
                                </div>
                                <div class="space-6"></div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label id="Label5" class="pull-left">
                                            <h5>Morning Time</h5>
                                        </label>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="input-group bootstrap-timepicker">
                                            <input id="MornigTime" type="text" class="form-control"/>

                                        </div>
                                    </div>
                                     <div class="col-lg-3">
                                        <label id="Label6" class="pull-left">
                                            <h5>Evening Time</h5>
                                        </label>
                                    </div>
                                    <div class="col-lg-3 ">

                                        <div class="input-group bootstrap-timepicker">
                                            <input id="timepicker1" type="text" class="form-control"/>

                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="space-6"></div>
                                   <div class="row">
                                   <div class="col-lg-3">
                                        <label id="Label7" class="pull-left" >
                                            <h5>Morning KM</h5>
                                        </label>
                                    </div>
                                    <div class="col-lg-3">
                                        <input type="text" id="txtMorningKM" class="Numaric required form-control margin-top-4" tabindex="5" data-toggle="SpnMorKM"/>
                                    </div>
                                    <div class="col-lg-3">
                                        <label id="Label8" class="pull-left">
                                            <h5>Evening KM.</h5>
                                        </label>
                                    </div>
                                    <div class="col-lg-3">
                                        <input type="text" id="txtEveKM" class="Numaric required form-control margin-top-4" tabindex="6" data-toggle="SpnEvnKM"/>
                                    </div>
                                   
                                </div>
                              <div class="row">
                                    <div class="col-lg-6">
                                        <span class="red margin-bottom-4 input pull-right" id="SpnMorKM" style="display: none;"><i class="ace-icon red bigger-140 fa fa-times-circle"></i>&nbsp;Number Must Required</span>
                                    </div>
                                    <div class="col-lg-6">
                                        <span class="red margin-bottom-4 input pull-right" id="SpnEvnKM" style="display: none;"><i class="ace-icon red bigger-140 fa fa-times-circle"></i>&nbsp;Number Must Required</span>
                                    </div>
                                </div>
                                <div class="space-6"></div>
                                <div class="row">                                  
                                    <div class="col-lg-3">
                                        <label id="Label9" class="pull-left">
                                            <h5>Is Mix Milk</h5>
                                        </label>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>
                                            <input type="checkbox" id="ChkIsMilk" tabindex="7"/>
                                        </label>
                                    </div>

                                </div>
                                <div class="space-8"></div>
                                <div class="row">
                                    <div class="col-lg-3">
                                    </div>
                                    <div class="col-lg-3">
                                        <button class="btn btn-white btn-danger btn-bold bigger-130" id="btnBack" type="button" style="width:100%" onclick="BtnBack_Click()" value="UPDATE" >
                                            <i class="ace-icon fa fa-arrow-left red"></i>&nbsp;Back
                                        </button>

                                        
                                    </div>
                                    <div class="col-lg-3 ">
                                        <button class="btn btn-white btn-success btn-bold bigger-130" style="width:120%" id="btnSave" type="button" onclick="btnSave_click()" value="Save" tabindex="8">
                                            <i class="ace-icon fa fa-floppy-o green"></i>&nbsp;Submit & New&nbsp;
                                        </button>
                                    </div>
                                    <div class="col-lg-3">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                
            </div>
            
        </div>
        <div class="spinner" id="divspinner"></div>
        <div class="row"><div id="ohsnap"></div></div>
    </div>
    
</asp:Content>
