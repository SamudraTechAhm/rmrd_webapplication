﻿using RMRD.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RMRD
{
    public partial class TruckArrival : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static string GetTruckArrivalInfo()
        {
            BALTruckArriaval objBALTruckArriaval = null;
            try
            {
                objBALTruckArriaval = new BALTruckArriaval();
                return objBALTruckArriaval.BALSelectData("SP_DropDownList", "BindRoutedetails");
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally { objBALTruckArriaval = null; }
        }
        [WebMethod]
        public static string UpdateSocietyDataByID(BALTruckArriaval TruckArriaval,string spName)
        {
            BALTruckArriaval objBALSoceityMasterEdit = null;
            try
            {
                objBALSoceityMasterEdit = new BALTruckArriaval();
                return objBALSoceityMasterEdit.BALUpdateData(spName, TruckArriaval);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally { objBALSoceityMasterEdit = null; }
        }
    }
}