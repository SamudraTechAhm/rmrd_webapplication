﻿using RMRD.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RMRD
{
    public partial class ContractorMasterEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static string LoadToForm(Int64 RtcCode)
        {
            BALContractorMaster objBALContractorMaster = null;
            string JSON = string.Empty;
            try
            {
                objBALContractorMaster = new BALContractorMaster();
                JSON = objBALContractorMaster.BALSelectAllContractor("SpRMRDContractorMaster", RtcCode);
                return JSON.Substring(1, JSON.Length - 2);
            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                objBALContractorMaster = null;
            }
        }
        [WebMethod]
        public static string InsertSuprevisorDetail(BALContractorMaster Contractor)
        {
            try
            {
                //Supervisior.CntCode = Convert.ToInt64(HttpContext.Current.Session["UserCode"].ToString());
                //Supervisior.CenterCode = Convert.ToInt64(HttpContext.Current.Session["UserCode"].ToString());
                //Supervisior.CId = Convert.ToInt64(HttpContext.Current.Session["UserCode"].ToString());
                return Contractor.BALInsertAndUpdate("SpRMRDContractorMaster", Contractor);
            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                Contractor = null;
            }
        } 
    }
}