﻿using RMRD.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RMRD
{
    public partial class RouteMasterEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static string LoadToForm(string RouteCode)
        {
            BALRouteMaster objBALRouteMaster = null;
            string JSON = string.Empty;
            try
            {
                objBALRouteMaster = new BALRouteMaster();
                JSON = objBALRouteMaster.BALSelectRouteDetail("Sp_RoughtMaster", Convert.ToInt64(RouteCode));
                return JSON.Substring(1, JSON.Length - 2);
            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {

            }
        }
        
        [WebMethod]
        public static string SelectData()
        {
            BALRouteMaster objBALRouteMaster = null;
            string JSON = string.Empty;
            try
            {
                objBALRouteMaster = new BALRouteMaster();
                return JSON = objBALRouteMaster.BALSelectData("Sp_RoughtMaster");
                //return JSON.Substring(1, JSON.Length - 2);
            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {

            }
        }
        //InsertRouteDetail
        [WebMethod]
        public static void InsertRouteDetail(BALRouteMaster Route)
        {
            try
            {
                //Route.CrID = Convert.ToInt64(HttpContext.Current.Session["UserCode"].ToString());
                //Route.CrDate = DateTime.Now.ToString("yyyy/MM/dd");
                Route.BALInsertAndUpdate("Sp_RoughtMaster", Route);
            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                Route = null;
            }
        }  
    }

}
