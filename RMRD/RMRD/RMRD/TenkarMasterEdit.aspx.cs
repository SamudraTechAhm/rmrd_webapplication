﻿using RMRD.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RMRD
{
    public partial class TenkarMasterEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static string LoadToForm(Int64 VehicalCode)
        {
            BALTenkerMaster objBALTenkerMaster = null;
            string JSON = string.Empty;
            try
            {
                objBALTenkerMaster = new BALTenkerMaster();
                JSON = objBALTenkerMaster.BALSelectAll("SpRMRDTenkarMaster", VehicalCode);
                return JSON.Substring(1, JSON.Length - 2);
            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                objBALTenkerMaster = null;
            }
        }
        [WebMethod]
        public static string InsertUpdateDelete(BALTenkerMaster Tenkar)
        {
            try
            {
                //Supervisior.CntCode = Convert.ToInt64(HttpContext.Current.Session["UserCode"].ToString());
                //Supervisior.CenterCode = Convert.ToInt64(HttpContext.Current.Session["UserCode"].ToString());
                //Supervisior.CId = Convert.ToInt64(HttpContext.Current.Session["UserCode"].ToString());
                return Tenkar.BALInsertAndUpdate("SpRMRDTenkarMaster", Tenkar);
            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                Tenkar = null;
            }
        } 
    }
}