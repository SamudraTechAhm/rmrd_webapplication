﻿using RMRD.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RMRD
{
    public partial class TenkerMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static string GetTenkar()
        {
            BALTenkerMaster objBALTenkerMaster = null;
            try
            {
                objBALTenkerMaster = new BALTenkerMaster();
                return objBALTenkerMaster.BALSelectAll("SpRMRDTenkarMaster", 0);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        [WebMethod]
        public static string InsertUpdateDelete(BALTenkerMaster area)
        {
            try
            {
                //Supervisior.CntCode = Convert.ToInt64(HttpContext.Current.Session["UserCode"].ToString());
                //Supervisior.CenterCode = Convert.ToInt64(HttpContext.Current.Session["UserCode"].ToString());
                //Supervisior.CId = Convert.ToInt64(HttpContext.Current.Session["UserCode"].ToString());
                return area.BALInsertAndUpdate("SpRMRDTenkarMaster", area);
            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                area = null;
            }
        } 
    }
}