﻿$(document).ready(function () {
    $(".Focus").click(function () {
        var id = $(this).attr('id');
        if (id == "A2")
            $("#T2R1C1").focus();
    });

    $(".required").blur(function () {
        var id = $(this).attr('id');
        //alert(id);
        var spanId = $("#" + id).attr('data-toggle');
        var tag = document.getElementById(id).tagName; //$("#" + id).get(0).tagName;
        //alert(tag);
        if (tag == "SELECT") {
            
            if ($("#" + id).val().trim() == "0") {
                $("#" + spanId).show();
            }
            else $("#" + spanId).hide();
        }
        else if (tag == "INPUT") {
            if ($("#" + id).val().trim() == "")
                $("#" + spanId).show();
            else $("#" + spanId).hide();
        }
    });
   
    $(".Email").blur(function () {
        var id = $(this).attr('id');
        var spanId = $("#" + id).attr('data-toggle');
        //alert("#" + spanId);
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if ($("#" + id).val().trim() != "") {

            if (reg.test($("#" + id).val()) == false) {
                //alert("invalid")
                $("#" + spanId).html("<i class=\"ace-icon red bigger-140 fa fa-times-circle\"></i>" + "&nbsp;Invalid Email").show();
            }
        }
    });

    $(".Numaric").keyup(function (e) {
        var num = 0;
        num = e.key;
        var Id = $(this).attr("id");
        // alert(Id);
        if (num == 0 || num == 1 || num == 2 || num == 3 || num == 4 || num == 5 || num == 6 || num == 7 || num == 8 || num == 9) {
            var TextVal = $("#" + Id).val().trim();
            $("#" + Id).val(TextVal);
        }
        else if (e.keyCode >= 65 && e.keyCode <= 90) {
            $("#" + Id).val("");
        }
    });

    $(".Float").keyup(function (e) {
        var num = 0;
        num = e.key;
        var Id = $(this).attr("id");
        // alert(Id);
        if (num == 0 || num == 1 || num == 2 || num == 3 || num == 4 || num == 5 || num == 6 || num == 7 || num == 8 || num == 9 || num == '.') {
            var TextVal = $("#" + Id).val().trim();
            $("#" + Id).val(TextVal);
        }
        else if (e.keyCode >= 65 && e.keyCode <= 90) {
            $("#" + Id).val("");
        }
    });

});

function Warning() {
    //$('#Warning').dialog("resize", "auto");
    var dialog = $("#Warning").removeClass('hide').dialog({
        width: "400px",
        minHeight: 'auto',
        modal: false,
        title_html: false,
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
        }
    }).dialog("widget").find(".ui-dialog-title").hide();
}