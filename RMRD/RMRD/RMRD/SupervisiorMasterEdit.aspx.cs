﻿using RMRD.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RMRD
{
    public partial class SupervisiorMasterEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static string LoadToForm(Int64 FsCode)
        {
            BALSupervisiorMaster objBALSupervisiorMaster = null;
            string JSON = string.Empty;
            try
            {
                objBALSupervisiorMaster = new BALSupervisiorMaster();
                JSON = objBALSupervisiorMaster.BALSelectAllSuperVisiour("SpRMRDSupervisiorMaster", FsCode);
                return JSON.Substring(1, JSON.Length - 2);
            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                objBALSupervisiorMaster = null;
            }
        }

        [WebMethod]
        public static string InsertSuprevisorDetail(BALSupervisiorMaster Supervisior)
        {
            try
            {
                //Supervisior.CntCode = Convert.ToInt64(HttpContext.Current.Session["UserCode"].ToString());
                //Supervisior.CenterCode = Convert.ToInt64(HttpContext.Current.Session["UserCode"].ToString());
                //Supervisior.CId = Convert.ToInt64(HttpContext.Current.Session["UserCode"].ToString());
                return Supervisior.BALInsertAndUpdate("SpRMRDSupervisiorMaster", Supervisior);
            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                Supervisior = null;
            }
        } 
    }
}