﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.Master" AutoEventWireup="true" CodeBehind="SocietyMasterEdit.aspx.cs" Inherits="RMRD.SocietyMasterEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="External-JS/Object.js"></script>
    <link href="assets/css/Common-Style.css" rel="stylesheet" />
    <script src="assets/js/ohsnap.min.js"></script>
    <script src="assets/js/Validation/Validation.js"></script>
    <style>
        input[type=text], textarea, select {
            -webkit-transition: all 0.30s ease-in-out;
            -moz-transition: all 0.30s ease-in-out;
            -o-transition: all 0.30s ease-in-out;
            outline: none;
            padding: 3px 0px 3px 3px;
            margin: 5px 1px 3px 0px;
            border: 2px solid #DDDDDD;
        }

        input[type=text], textarea, select {
            -webkit-transition: all 0.30s ease-in-out;
            -moz-transition: all 0.30s ease-in-out;
            -o-transition: all 0.30s ease-in-out;
            outline: none;
            padding: 3px 0px 3px 3px;
            margin: 5px 1px 3px 0px;
        }

            input[type=text]:focus, textarea:focus, select:focus {
                box-shadow: 0 0 5px rgba(81, 203, 238, 1);
                padding: 3px 0px 3px 3px;
                margin: 5px 1px 3px 0px;
                border: 2px solid red;
            }

        .alert1 {
            padding: 5px;
            margin-bottom: 5px;
            border: 1px solid #eed3d7;
            border-radius: 4px;
            position: absolute;
            bottom: 0px;
            right: 21px;
            /* Each alert has its own width */
            float: right;
            clear: right;
            color: white;
            background-color: rgba(81, 203, 238, 1);
        }

        }

        .spinner {
            display: none;
            position: fixed;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background: rgba( 255, 255, 255, .4 ) url('Images/spinner.gif') 50% 50% no-repeat;
            overflow: hidden;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {

            $("#ddlRouteDetails").change(function () {
                var value = $('#ddlRouteDetails').val();
                $("#txtRouteCode").val(value);
            });

            $("#txtRouteCode").attr("disabled", "disabled");
            var ID = GetParameterValues('id');
            if (ID != null) {
                LoadData(ID);
                $('#BtnSave').val('UPDATE');
                $('#BtnSave').text('UPDATE')
            }
            else {
                FillDropDown();
                AutoEnableDisableSocCode();
            }
            $divspinner = $("#divspinner");
            $(document).ajaxStart(function () {
                $divspinner.show();
            });
            $(document).ajaxStop(function () {
                setTimeout($divspinner.hide(), 50000);
            });

        });
        function AutoEnableDisableSocCode() {
            $.ajax({
                type: "POST",
                url: "SocietyMasterEdit.aspx/GetSocietySetting",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    var json = JSON.parse(data.d);

                    $.each(json, function (key, value) {
                        if (value.SocCode > 0) {
                            $('#txtRouteID').val(value.SocCode);
                            $("#txtRouteID").attr("disabled", "disabled");
                        }
                    });
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
        function FillDropDown() {
            var action = null;
            action = "BindRouteNameTalukaName";
            $.ajax({
                type: "POST",
                url: "Dropdown.aspx/BindDropDownList",
                data: "{ Action: '" + action + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    var TableGroupName = data.d;
                    var TableArray = TableGroupName.split('^');
                    var JsonTalukaDetails = jQuery.parseJSON(TableArray[0]);
                    var jsonRouteDetails = jQuery.parseJSON(TableArray[1]);


                    $.each(jsonRouteDetails, function (key, value) {
                        $("#ddlRouteDetails").append($("<option></option>").val(value.Code).html(value.Name));
                    });

                    $.each(JsonTalukaDetails, function (key, value) {
                        $("#ddlTalukaDetails").append($("<option></option>").val(value.TalukaCode).html(value.TalukaName));
                    });




                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
        function LoadData(ID) {
            $.ajax({
                type: "POST",
                url: "SocietyMasterEdit.aspx/GetSocietyDataByID",
                data: '{SocCode: "' + ID + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    var TableGroupName = data.d;
                    var TableArray = TableGroupName.split('^');
                    var JsonSocietyDetails = jQuery.parseJSON(TableArray[0]);
                    var jsonRouteDetails = jQuery.parseJSON(TableArray[1]);
                    var jsonTalukaDetails = jQuery.parseJSON(TableArray[2]);

                    $.each(jsonRouteDetails, function (key, value) {
                        $("#ddlRouteDetails").append($("<option></option>").val(value.RtCode).html(value.RtName));
                    });

                    $.each(jsonTalukaDetails, function (key, value) {
                        $("#ddlTalukaDetails").append($("<option></option>").val(value.tlkCode).html(value.tlkName));
                    });
                    $.each(JsonSocietyDetails, function (key, value) {
                        $("#ddlRouteDetails").val(value.rtCode);
                        $("#ddlTalukaDetails").val(value.tlkCode);
                        $("#txtRouteID").val(value.SocCode);
                        $("#txtRouteID").attr("disabled", "disabled");
                        $("#txtSocietyName").val(value.socName);
                        $("#txtRouteCode").val(value.rtCode);
                        $("#txtRouteCode").attr("disabled", "disabled");
                        $("#txtMobileNo").val(value.mobileno);

                    });


                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
        function BtnSaveClick() {

            var operation = $('#BtnSave').attr('value');
            
            if (operation == "Save") {

                SaveData("INSERT");
            }
            else if (operation == "UPDATE") {
                SaveData("UPDATE");
            }
        }
        function SocietyMasterEditObjCreate() {
            var _SocietyMasterEdit = {};
            _SocietyMasterEdit.Row_ID = 0,
            _SocietyMasterEdit.SocCode = 0,
            _SocietyMasterEdit.socName = '',
            _SocietyMasterEdit.Action = '',
            _SocietyMasterEdit.socName2 = '',
            _SocietyMasterEdit.soccodetxt = '',
            _SocietyMasterEdit.rtCode = 0,
            _SocietyMasterEdit.fsCode = 0,
            _SocietyMasterEdit.tlkCode = 0,
            _SocietyMasterEdit.CId = 0,
            _SocietyMasterEdit.mobileno = '',
            _SocietyMasterEdit.IsManual = 2
            return _SocietyMasterEdit;
        }
        function UpdateRecord() {
            var SocietyMasterEdit = SocietyMasterEditObjCreate();
            SocietyMasterEdit.SocCode = $('#txtRouteID').val();
            SocietyMasterEdit.socName = $('#txtSocietyName').val();
            SocietyMasterEdit.rtCode = $('#txtRouteCode').val();
            SocietyMasterEdit.mobileno = $('#txtMobileNo').val();
            SocietyMasterEdit.tlkCode = $('#ddlTalukaDetails').val();
            SocietyMasterEdit.Action = "UPDATE";

            $.ajax({
                type: "POST",
                url: "SocietyMasterEdit.aspx/UpdateSocietyDataByID",
                data: JSON.stringify({ 'SocietyMasterEdit': SocietyMasterEdit }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {

                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
        function SaveData(action) {
            if ($("#txtRouteID").val().trim() == "" || $("#txtSocietyName").val().trim() == "" || $("#txtRouteCode").val().trim() == "" || $("#ddlRouteDetails").val().trim() == "0" || $("#txtMobileNo").val().trim() == "" || $("#ddlTalukaDetails").val().trim() == "0") {
                $(".required").blur();
            }
            else {
                var SocietyMasterEdit = SocietyMasterEditObjCreate();
                SocietyMasterEdit.SocCode = $('#txtRouteID').val();
                SocietyMasterEdit.socName = $('#txtSocietyName').val();
                SocietyMasterEdit.rtCode = $('#txtRouteCode').val();
                SocietyMasterEdit.mobileno = $('#txtMobileNo').val();
                SocietyMasterEdit.tlkCode = $('#ddlTalukaDetails').val();
                SocietyMasterEdit.Action = action;
                if ($('#txtRouteID').prop('disabled')) {
                    SocietyMasterEdit.IsManual = 0
                }
                if (!($('#txtRouteID').prop('disabled'))) {
                    SocietyMasterEdit.IsManual = 1
                }

                $.ajax({
                    type: "POST",
                    url: "SocietyMasterEdit.aspx/UpdateSocietyDataByID",
                    data: JSON.stringify({ 'SocietyMasterEdit': SocietyMasterEdit }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    success: function (data) {
                        ohSnap(data.d);

                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }
        }


    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="breadcrumbs center" id="breadcrumbs">
        <ul class="breadcrumb ">
            <li>
                <%--<i class="ace-icon fa  fa-asterisk"></i>--%>
                <span style="font-family: Verdana; font-size: 20px; color: deepskyblue">SOCIETY DETAIL</span>
            </li>

        </ul>

    </div>
    <div class="row">
        <div class="col-lg-2 col-sm-2"></div>
        <div class="col-lg-8 col-sm-8">
            <div class="widget-box widget-color-blue ui-sortable-handle">
                <div class="widget-header">
                    <h5 class="widget-title">SOCEITY DETAILS</h5>

                    <div class="widget-toolbar">
                        <a href="#" data-action="collapse">
                            <i class="1 ace-icon fa fa-chevron-up bigger-125"></i>
                        </a>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="widget-main alert alert-info">
                        <div class="row">
                            <div class="col-lg-2">
                                <label id="lblEmployeName" class="pull-left">
                                    <h5>Society ID</h5>
                                </label>
                            </div>
                            <div class="col-lg-4">
                                <input type="text" id="txtRouteID" class=" required Numaric form-control margin-top-4" tabindex="0" data-toggle="SpnSocCode"/>
                            </div>
                            <div class="col-lg-2">
                                <label id="LblSocietyName" class="pull-left">
                                    <h5>Society Name</h5>
                                </label>
                            </div>
                            <div class="col-lg-4">
                                <input type="text" id="txtSocietyName" class="required form-control margin-top-4" tabindex="0" data-toggle="spnSocName"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-4">
                                <span class="red margin-bottom-4 input pull-right" id="SpnSocCode" style="display: none;"><i class="ace-icon red bigger-140 fa fa-times-circle"></i>&nbsp;Soc ID Is Required</span>
                            </div>
                            <div class="col-lg-2"></div>
                            <div class="col-lg-4">
                                <span class="red margin-bottom-4 input pull-right" id="spnSocName" style="display: none;"><i class="ace-icon red bigger-140 fa fa-times-circle"></i>&nbsp;Soc Name Is Required</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2">
                                <label id="Label8" class="pull-left">
                                    <h5>Route Code</h5>
                                </label>
                            </div>
                            <div class="col-lg-4">
                                <input type="text" id="txtRouteCode" class="required numeric form-control margin-top-4" tabindex="0"  data-toggle="SpntxtRouteCode"/>
                            </div>
                            <div class="col-lg-2">
                                <label id="Label9" class="pull-left">
                                    <h5>Route Name</h5>
                                </label>
                            </div>
                            <div class="col-lg-4">
                                <select id="ddlRouteDetails" class="form-control required" data-toggle="SpnddlRouteDetails" tabindex="2">
                                    <option value="0">SELECT</option>
                                </select>
                            </div>

                        </div>
                           <div class="row">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-4">
                                <span class="red margin-bottom-4 input pull-right" id="SpntxtRouteCode" style="display: none;"><i class="ace-icon red bigger-140 fa fa-times-circle"></i>&nbsp;Route Code Is Required</span>
                            </div>
                            <div class="col-lg-2"></div>
                            <div class="col-lg-4">
                                <span class="red margin-bottom-4 input pull-right" id="SpnddlRouteDetails" style="display: none;"><i class="ace-icon red bigger-140 fa fa-times-circle"></i>&nbsp;Route Name Is Required</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2">
                                <label id="Label10" class="pull-left">
                                    <h5>Mobile No</h5>
                                </label>
                            </div>
                            <div class="col-lg-4">
                                <input type="text" id="txtMobileNo" class="required numeric form-control margin-top-4" tabindex="0" data-toggle="SpntxtMobileNo" maxlength="10"/>
                            </div>
                            <div class="col-lg-2">
                                <label id="Label11" class="pull-left">
                                    <h5>Tauka Name</h5>
                                </label>
                            </div>
                            <div class="col-lg-4">
                                <select id="ddlTalukaDetails" class="form-control required" data-toggle="SpnddlTalukaDetails" tabindex="2">
                                    <option value="0">SELECT</option>
                                </select>
                            </div>

                        </div>
                          <div class="row">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-4">
                                <span class="red margin-bottom-4 input pull-right" id="SpntxtMobileNo" style="display: none;"><i class="ace-icon red bigger-140 fa fa-times-circle"></i>&nbsp;Mobile No Is Required</span>
                            </div>
                            <div class="col-lg-2"></div>
                            <div class="col-lg-4">
                                <span class="red margin-bottom-4 input pull-right" id="SpnddlTalukaDetails" style="display: none;"><i class="ace-icon red bigger-140 fa fa-times-circle"></i>&nbsp;Tauka Name Is Required</span>
                            </div>
                        </div>

                    </div>

                    <div class="widget-toolbox padding-8 clearfix">
                        <div class="row">
                            <div class="col-lg-4">

                                <button class="btn btn-xs btn-danger pull-left" type="button" id="btnBack" onclick="BackClick()">
                                    <i class="ace-icon fa fa-arrow-left icon-on-left"></i>
                                    <span class="bigger-110">Back</span>
                                </button>
                            </div>
                            <div class="col-lg-4 col-sm-4">
                                <br />
                                <br />
                                <div id="ohsnap"></div>
                            </div>
                            <div class="col-lg-4">

                                <button class="btn btn-xs btn-info  pull-right" type="button" onclick="BtnSaveClick()" id="BtnSave" value="Save">
                                    <i class="ace-icon fa fa-floppy-o white"></i>
                                    <span class="bigger-110">SAVE</span>

                                </button>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        <div class="col-lg-2 col-sm-2"></div>
    </div>


    <div class="spinner" id="divspinner"></div>

</asp:Content>
