﻿using RMRD.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace RMRD
{
    public partial class SocietyMasterEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static string GetSocietySetting()
        {
            BALSocietyMasterEdit objBALSoceityMasterEdit = null;
            try
            {
                objBALSoceityMasterEdit = new BALSocietyMasterEdit();
                return objBALSoceityMasterEdit.BALSelectData("SpRMRDSoceityMaster");
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally { objBALSoceityMasterEdit = null; }
        }
        [WebMethod]
        public static string GetSocietyDataByID(Int64 SocCode)
        {
            BALSocietyMasterEdit objBALSoceityMasterEdit = null;
            try
            {
                objBALSoceityMasterEdit = new BALSocietyMasterEdit();
                return objBALSoceityMasterEdit.BALSelectData("SpRMRDSoceityMaster",SocCode);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally { objBALSoceityMasterEdit = null; }
        }
        [WebMethod]
        public static string UpdateSocietyDataByID(BALSocietyMasterEdit SocietyMasterEdit)
        {
            BALSocietyMasterEdit objBALSoceityMasterEdit = null;
            try
            {
                objBALSoceityMasterEdit = new BALSocietyMasterEdit();
                return objBALSoceityMasterEdit.BALUpdateData("SpRMRDSoceityMaster", SocietyMasterEdit);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally { objBALSoceityMasterEdit = null; }
        }
    }
}