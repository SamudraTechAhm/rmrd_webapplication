﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.Master" AutoEventWireup="true" CodeBehind="SoceitySetting.aspx.cs" Inherits="RMRD.SoceitySetting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="assets/js/ohsnap.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            SocietySettings();
            $('#ChkIsAutomatic').change(function () {
                if ($(this).is(":checked")) {
                    $("#ChkIsManual").prop("checked", false);
                    SaveData(1, 0);
                }
                else {
                    $("#ChkIsManual").prop("checked", true);
                    SaveData(0, 1);
                }

            });

            $('#ChkIsManual').change(function () {
                if ($(this).is(":checked")) {
                    $("#ChkIsAutomatic").prop("checked", false);
                    SaveData(0, 1);
                }
                else {
                    $("#ChkIsAutomatic").prop("checked", true);
                    SaveData(1, 0);
                }

            });
        });
        function SaveData(IsAutomatic, IsManuall) {
            
            var SocietySeting = SocietySetingObjCreate();
            SocietySeting.IsAutomatic = IsAutomatic;
            SocietySeting.IsManual = IsManuall;
            $.ajax({
                type: "POST",
                url: "SoceitySetting.aspx/UpdateSocietySetting",
                data: JSON.stringify({ 'SocietySeting': SocietySeting }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    ohSnap('Society Setting Confirgure Successfully !!');
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
        function SocietySetingObjCreate() {
            var _SocietySeting = {};
            _SocietySeting.IsManual = 0;
            _SocietySeting.IsAutomatic = 0;
            return _SocietySeting;
        }
        function SocietySettings() {
            $.ajax({
                type: "POST",
                url: "SoceitySetting.aspx/GetSocietySetting",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (data) {
                    var json = JSON.parse(data.d);
                    var ismanuall, isautomatic;
                    $.each(json, function (key, value) {
                        ismanuall = value.IsManual;
                        isautomatic = value.IsAutomatic;
                    });
                    if (ismanuall == 0) {
                        $("#ChkIsManual").prop("checked", false);
                    }
                    if (ismanuall == 1) {

                        $("#ChkIsManual").prop("checked", true);
                    }
                    if (isautomatic == 0) {

                        $("#ChkIsAutomatic").prop("checked", false);
                    }
                    if (isautomatic == 1) {
                        $("#ChkIsAutomatic").prop("checked", true);
                    }

                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
    </script>
    <style>
        .alert1 {
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid #eed3d7;
            border-radius: 4px;
            position: absolute;
            bottom: 0px;
            right: 21px;
            /* Each alert has its own width */
            float: left;
            clear: left;
            color: white;
            background-color: green;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--<div class="breadcrumbs center" id="breadcrumbs">
        <ul class="breadcrumb ">
            <li>
                <i class="ace-icon fa  fa-asterisk"></i>
                <span style="font-family: Verdana; font-size: 20px; color: deepskyblue">SOCIETY SETTING INFORMATION DETAIL</span>
          
            </li>
            </br>
                <li>
                    <i class="ace-icon fa fa-check blue"></i>
                    Welcome to <strong class="info">Society Code Generate Automatically</strong>OR<strong class="red">Society Code Generate Manually</strong>Setting
                </li>
        </ul>
    </div>--%>
    <div class="widget-main  center">
        
        <div class="alert alert-info" style="font-family: Verdana; font-size: 18px; color: deepskyblue"><i class="ace-icon fa fa-check blue"></i>&nbsp;&nbsp;Welcome To <B>Society Code Generate Automatically</B> OR <B>Society Code Generate Manually</B> Settings</div>
    </div>
    <div class="row">
       

        <div class="widget-box widget-color-blue ui-sortable-handle">
            <div class="widget-header">
                <h5 class="widget-title">Society Setting</h5>
                <div class="widget-toolbar">
                    <a href="#" data-action="collapse">
                        <i class="1 ace-icon fa fa-chevron-up bigger-125"></i>
                    </a>
                </div>

            </div>
            <div class="widget-body ">
                <div class="widget-main alert alert-info">
                    <div class="row ">
                        <div class="col-lg-6">
                            <span class="col-sm-8">
                                <label class="pull-right inline">
                                    <small class="muted smaller-90"><strong class="black">Society Code Generate Automatically By System</strong></small>

                                    <input id="ChkIsAutomatic" checked="checked" class="ace ace-switch ace-switch-5" type="checkbox">
                                    <span class="lbl middle"></span>
                                </label>
                            </span>
                        </div>
                        <div class="col-lg-6">

                            <span class="col-sm-8">
                                <label class="pull-right inline">
                                    <small class="muted smaller-90"><strong class="black">Society Code Generate Manually By Human</strong></small>

                                    <input id="ChkIsManual" checked="checked" class="ace ace-switch ace-switch-5" type="checkbox">
                                    <span class="lbl middle"></span>
                                </label>
                            </span>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <div class="spinner" id="divspinner"></div>
    <br />
    <br />
    <br />
    <br />
    <br />
    <div id="ohsnap"></div>


</asp:Content>
