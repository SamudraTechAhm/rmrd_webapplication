﻿using RMRD.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace RMRD
{
    public partial class MobileEdit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static string GetSocietyDataByID(Int64 SocCode)
        {
            BALMobileMaster objMobileMaster = null;
            try
            {
                objMobileMaster = new BALMobileMaster();
                return objMobileMaster.BALSelectData("spRMRDMobileMasterEdit", SocCode);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally { objMobileMaster = null; }
        }
        [WebMethod]
        public static string UpdateSocietyDataByID(BALSocietyMasterEdit SocietyMasterEdit)
        {
            BALSocietyMasterEdit objBALSoceityMasterEdit = null;
            BALMobileMaster objMobilemaster = new BALMobileMaster();
            try
            {
                objBALSoceityMasterEdit = new BALSocietyMasterEdit();
                return objMobilemaster.BALUpdateData("spRMRDMobileMasterEdit", SocietyMasterEdit);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally { objBALSoceityMasterEdit = null; objMobilemaster = null; }
        }
    }
}