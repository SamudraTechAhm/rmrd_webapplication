﻿using RMRD.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CFAS_Web_App
{
    public partial class DropDown : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        
        [WebMethod]
        public static string BindDropDownList(string Action = null)
        {
            BALDropDownList baldropdownlist = null;
            try
            {
                baldropdownlist = new BALDropDownList();

                return baldropdownlist.BALSelectDropDownList(Action);
                //return Json;
            }
            catch (Exception ex)
            {
                throw ex.GetBaseException();
            }
            finally
            {
                baldropdownlist = null;
            }
        }
    }
}