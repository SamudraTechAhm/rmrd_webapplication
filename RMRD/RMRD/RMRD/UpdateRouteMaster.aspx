﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.Master" AutoEventWireup="true" CodeBehind="UpdateRouteMaster.aspx.cs" Inherits="RMRD.UpdateRouteMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="External-JS/UpdateRouteMaster.js"></script>
    <script src="External-JS/Paging.js"></script>
    <link href="assets/css/Common-Style.css" rel="stylesheet" />
    <style>
        .abc {
            height: 150px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="breadcrumbs center" id="breadcrumbs">

        <ul class="breadcrumb ">
            <li>
                <i class="ace-icon fa  fa-asterisk"></i>
                <a href="#" style="font-family: Calibri; font-size: larger">UPDATE ROUTE MASTER</a>
            </li>

        </ul>
    </div>
    <div class="row">
        <div class="col-lg-12 col-sm-12">
            <div class="widget-box widget-color-blue ui-sortable-handle">
                <div class="widget-header">
                    <h5 class="widget-title">Update Route Detail</h5>

                    <div class="widget-toolbar">
                        <a href="#" id="abc" data-action="collapse">
                            <i class="1 ace-icon fa fa-chevron-up bigger-125"></i>
                        </a>
                    </div>

                    <div class="widget-toolbar no-border">

                        <div id="nav-search" class="nav-search">
                            <form class="form-search">

                                <span class="input-icon">
                                    <%--<i class="ace-icon fa fa-search nav-search-icon"></i>--%>
                                    <input placeholder="Search..." type="text" id="nav-search-input" class="nav-search-input Search" data-toggle="TblUpdateRouteDetail" autocomplete="off" />
                                    <%--<input id="nav-search-input" class="nav-search-input" placeholder="Search ..." autocomplete="off" type="text">--%>
                                    
                                </span>
                            </form>
                        </div>

                    </div>
                </div>
                <div class="widget-body">
                    <div class="widget-main ">
                        <div class="box-body scroll--200 abc">
                            <table id="TblUpdateRouteDetail" class="table ">
                                <thead class="bg-blue-light">
                                    <tr>
                                        <th class="td center"></th>
                                        <th class="td center">Route Code</th>
                                        <th class="td center">Route Name</th>
                                        <th class="td center">Morning Time</th>
                                        <th class="td center">Evening Time</th>
                                        <th class="td center">Effective Date</th>
                                        <th class="td center">Status</th>

                                    </tr>
                                </thead>
                                <tbody class=""></tbody>
                            </table>
                        </div>
                        <div class="center">
                            <div class="space-4 tbl-footer-bg"></div>
                            <div class="tbl-footer-bg">
                                <button onclick="FirstPage('TblUpdateRouteDetail')" class="margin-bottom-4 margin-top-4 btn btn-sm btn-tumblr" type="button"><i class="ace-icon fa fa-angle-double-left"></i></button>
                                <button onclick="PreviousPage('TblUpdateRouteDetail')" class="margin-bottom-4 margin-top-4 btn btn-sm btn-tumblr" type="button"><i class="ace-icon fa fa-angle-left"></i></button>
                                <b class="bigger-100">|</b>&nbsp;&nbsp;<span dir="ltr">Page<span id="sp_0_grid-pager">1</span>of <span id="sp_1_grid-pager">23</span></span>
                                <b class="bigger-100">|</b>&nbsp;&nbsp;
                                             <button onclick="NextPage('TblUpdateRouteDetail')" class="margin-bottom-4 margin-top-4 btn btn-sm btn-tumblr" type="button"><i class="ace-icon fa fa-angle-right"></i></button>
                                <button onclick="LastPage('TblUpdateRouteDetail')" class="margin-bottom-4 margin-top-4 btn btn-sm btn-tumblr" type="button"><i class="ace-icon fa fa-angle-double-right"></i></button>
                            </div>
                            <div class="space-4 tbl-footer-bg"></div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 col-sm-12">
                                <div class="breadcrumbs center" id="Div1">
                                    <ul class="breadcrumb ">
                                        <li>
                                          <i class="ace-icon fa  fa-asterisk"></i>
                                          <a href="#" style="font-family: Calibri; font-size: larger">UPDATE ROUTE MASTER</a>
                                        </li>

                                    </ul>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    parth
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    barot
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="widget-toolbox padding-8 clearfix">
                        <div class="row">
                            <div class="col-lg-6">

                                <button class="btn btn-xs btn-danger pull-left" type="button" id="btnBackToHome" onclick="BtnBackClick()">
                                    <i class="ace-icon fa fa-arrow-left icon-on-left"></i>
                                    <span class="bigger-110">HOME</span>
                                </button>
                            </div>
                            <div class="col-lg-6">
                                <button class="btn btn-xs btn-success pull-right" type="button" onclick="BtnNewClick()" id="BtnAddNew">
                                    <span class="bigger-110">New Entry</span>
                                    <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

</asp:Content>
